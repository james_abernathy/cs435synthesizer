// CS435 Project: MIDI-Controlled Synthesizer
// Program Written By: James Abernathy, Esra Bahceci, Angel Vazquez-Salazar
// File Written By: Angel Vazquez-Salazar
// Notes: This SampleBuffer file was intended to pre-define an array.
// Once this happens, we can reference the buffer in lieu of calculating
// the sine wave in real-time.
// We hope that this will speed up our program, and alleviate some of the stresses it currently has.

#include "SampleBuffer.hpp"
#include <cstddef>

const float PI_Value = 3.1415927;
const int Doubler_Value = 2;
const double Half_Value = 0.5f;

namespace Project
{
	SampleBuffer::SampleBuffer(const float radians){
		int array sample_buffer[360]; // Initialize buffer of 360 elements
		
		size_t position = (radians/(Doubler_Value*PI_Value)*sample_buffer.size()+Half_Value);
		return sample_buffer[position];
		
	} // End of Sample Buffer Class
} // End of Namespace Project