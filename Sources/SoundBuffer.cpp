/// @file
/// ::Project::SoundBuffer class for buffering soundwave output values.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include <cmath>
#include <cstdint>

#include "MidiIn/Types.hpp"

#include "SoundBuffer.hpp"


namespace Project
{
	SoundBuffer::SoundBuffer() :
		bufferWriteIndex_{0U},
		bufferReadIndex_{bufferWriteIndex_},
		refillNeeded_{true},  // Refill immediately
		refillTicker_{},
		bufferLeft_{},
		bufferRight_{}
	{
	}




	void SoundBuffer::writeSample(
		const float sampleLeft,
		const float sampleRight)
	{
		bufferLeft_[bufferWriteIndex_] = sampleLeft;
		bufferRight_[bufferWriteIndex_] = sampleRight;

		// Increment (and wrap) the write index using one assignment
		if (bufferWriteIndex_ >= bufferLength_ - 1U) {  // At end
			bufferWriteIndex_ = 0U;
		} else {
			++bufferWriteIndex_;
		}
	}




	void SoundBuffer::readSample(
		float &sampleLeft,
		float &sampleRight)
	{
		sampleLeft = bufferLeft_[bufferReadIndex_];
		sampleRight= bufferRight_[bufferReadIndex_];

		// Increment (and wrap) the read index using one assignment
		if (bufferReadIndex_ >= bufferLength_ - 1U) {  // At end
			bufferReadIndex_ = 0U;
		} else {
			++bufferReadIndex_;
		}
	}




	std::size_t SoundBuffer::countUnreadSamples() const
	{
		// Access the indices once since they are volatile
		const std::size_t readIndexOld{bufferReadIndex_};
		const std::size_t writeIndexOld{bufferWriteIndex_};

		if (writeIndexOld >= readIndexOld) {
			return writeIndexOld - readIndexOld;

		} else {
			// Circular queue contents wrap around to the start
			return writeIndexOld + 1U  // Samples near start of array
				+ (bufferLength_ - readIndexOld);  // Samples near end of array
		}
	}




	void SoundBuffer::start()
	{
		refillTicker_.attach(
			callback(this, &SoundBuffer::onRefillNeeded), refillPeriod_s_);
	}




	void SoundBuffer::onRefillNeeded()
	{
		refillNeeded_ = true;
	}




	bool SoundBuffer::refill(
		const MidiIn::State &midiState)
	{
		// π; Not standard enough for the standard library.
		static constexpr float pi{3.14159265358979323846f};

		// Raise perceptual sound volume percentage to this power to produce amplitude.
		//static constexpr float volumeToAmplitudePower{2.71828182845904523536f};  // Euler's number, e

		// Number of semitones, or half-steps, from one octave to the next.
		static constexpr float semitonesPerOctave{12.0f};

		// Time since the beginning, accumulated each sample period to pace sine-waves.
		static float time{0.0f};


		if (!refillNeeded_) {  // Not empty enough to need another refill
			return false;  // No update performed
		}
		refillNeeded_ = false;  // Until next #refillInterrupt()


		// Refill based on initial number of free spaces when called
		const std::size_t refillSampleCount{bufferLength_ - countUnreadSamples()};
		//const float refillDuration{samplePeriod_s_ * refillSampleCount};
		for (std::size_t index{0U};
			index < refillSampleCount;
			++index, time += samplePeriod_s_)
		{

			// Accumulate all component waveforms as one sample
			float sampleLeft{0.0f};
			float sampleRight{0.0f};

			// Calculate the next sound sample: PWM duty-cycles that represent line
			//   voltage to an amplifier and speakers
			for (MidiIn::ChannelIndex channelIndex{0U};
				channelIndex < midiState.numChannels_;
				++channelIndex)
			{
				const MidiIn::Channel &channel{*midiState.getChannel(channelIndex)};

				// Convert volume into non-linear amplitude to correspond to perception of loudness
				//const MidiIn::Volume channelVolume{channel.getVolume()};


				const MidiIn::NoteList &notes{channel.getNotes()};
				const MidiIn::NoteList::Index noteCount{notes.getSize()};
				for (MidiIn::NoteList::Index noteIndex{0U}; noteIndex < noteCount; ++noteIndex) {
					// Add note's waveform at the current time

					// Convert volume into non-linear amplitude to correspond to perception of loudness
					//const MidiIn::Volume noteVolume{channel.getEffectiveNotePressure(noteIndex)};
					//const MidiIn::Volume amplitudePercentage{
					//	std::pow(channelVolume * noteVolume, volumeToAmplitudePower)};

					const MidiIn::Note *note{notes.getNote(noteIndex)};
					if (!note) {
						puts("\tERROR: Note was removed from MIDI state while iterating.");
						return true;  // Partial update performed, maybe
					}

					// Correction factor for innacuracy in the oscillator.
					static constexpr float tuningFactor{440.0f / 448.0f};
					// Tuning standard that all notes are calculated relative to
					static constexpr float a4Frequency{440.0f * tuningFactor};
					static constexpr MidiIn::MessageData a4Semitone{69U};

					const float frequency{a4Frequency
						* std::pow(2.0f, (note->getSemitone() - a4Semitone) / semitonesPerOctave)};
					// Maintain sine range of [-1, 1] to allow destructive interference with other notes
					const float noteSample{std::sin(time * 2.0f * pi * frequency)};

					sampleLeft += noteSample;
					sampleRight += noteSample;
				}
			}

			// #############################################
			// Avoid clipping when notes constructively interfere to prevent harmonic distortion
			sampleLeft /= 4;
			sampleRight /= 4;

			// Scale from range [-1, 1] to [0, 1].
			sampleLeft = (sampleLeft + 1.0f) / 2.0f;
			sampleRight = (sampleRight + 1.0f) / 2.0f;

			writeSample(sampleLeft, sampleRight);
		}

		return true;  // Update performed
	}
}
