/// @file
/// The ::Project::Observable class for notifying ::Project::Observer%s when events occur.
///
/// @copyright Copyright © 2014-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef PROJECT_OBSERVABLE_HPP_
#define PROJECT_OBSERVABLE_HPP_


#include <cstdint>
#include "Uncopyable.hpp"
#include "Observer.hpp"


/// A subject that will notify ::Project::Observer%s when an event occurs.
/// @tparam TObservable  The child class of this Observable that should be
///   passed to ::Project::Observer%s on #notifyObservers().
template <typename TObservable>
class Observable :
	private Uncopyable
{


		/// Data type of ::Project::Observer indexes within an Observable.
	public:
		typedef std::uint8_t Index;




	/// Max number of simultaneous listening ::Project::Observer%s.
	///   Attempts to register more are discarded.
private:
	static constexpr Index observersMax_{4U};




	/// The index within #observers_ where the next registered ::Project::Observer will reside.
private:
	Index nextIndex_;

	/// A fixed array of registered ::Observer%s to be notified.
private:
	Observer<TObservable> *observers_[observersMax_];




	/// Finds the given ::Project::Observer's index within this Observable.
	/// @param[in] observer  The previously-registered observer to locate.
	/// @returns The index of @p observer, or an index beyond the last registered
	///   one if not found.
private:
	Index findObserver(
		const Observer<TObservable> &observer)
	{
		for (Index index{0U}; index < nextIndex_; ++index) {
			if (observers_[index] == &observer) {
				// Found registration entry
				return index;
			}
		}

		// Observer wasn't found
		return nextIndex_;
	}




	/// Initializes this Observable with no registered ::Project::Observer%s.
	///   It's protected to prevent instantiating an Observable directly.
protected:
	Observable() :
		nextIndex_{0U},
		observers_{}
	{
	}




	/// Notifies each registered ::Project::Observer of the change in this Observable.
	///   ::Project::Observer%s are notified in the reverse order of their 
	///   registration. This allows limited registrations and unregistrations to
	///   happen while notifying.
protected:
	void notifyObservers()
	{
		// Notify in reverse
		for (Index index{nextIndex_}; index > 0U; --index) {
			observers_[index - 1U]->notify(
				static_cast<TObservable &>(*this));
		}
	}




	/// Registers a ::Project::Observer instance to receive notifications from this subject.
	///   It is safe to register new ::Project::Observer%s during notification,
	///   but these new registrants won't receive the active notification.
	/// @param[in] observer  Interested listener to be notified.
	/// @returns `true` if the given ::Project::Observer was registered successfully.
public:
	bool registerObserver(
		Observer<TObservable> &observer)
	{
		if (nextIndex_ >= observersMax_) {  // Maximum observers already registered
			return false;  // Discard
		}

		if (findObserver(observer) >= nextIndex_) {  // Not already registered
			observers_[nextIndex_++] = &observer;  // Append new observer
		}
		return true;
	}




	/// Unregisters a ::Project::Observer from receiving notifications.
	/// @warn It is safe for a ::Project::Observer to unregister itself while
	///   being notified, **but unsafe to unregister any other ::Project::Observer**.
	/// @param[in] observer  The previously-registered listener to be removed.
	/// @returns `true` if the given ::Project::Observer was found and removed.
public:
	bool unregisterObserver(
		const Observer<TObservable> &observer)
	{
		const Index unregisterIndex{findObserver(observer)};
		if (unregisterIndex >= nextIndex_) {  // Not found
			return false;
		}

		--nextIndex_;
		// Shift following entries over top of the removed one
		for (Index index{unregisterIndex}; index < nextIndex_; ++index) {
			observers_[index] = observers_[index + 1U];
		}
		return true;  // Successfully removed
	}
};


#endif  // !defined(PROJECT_OBSERVABLE_HPP_)
