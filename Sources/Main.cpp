/// @file
/// Entry point of the Synthesizer firmware, ::main().
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "mbed/mbed.h"

#include "MidiIn/State.hpp"

#include "SoundBuffer.hpp"
#include "Speakers.hpp"
#include "UsbMidiDevice.hpp"

#include "SampleBuffer.hpp"



namespace {
	/// Cached state of MIDI values as messages are received.
	MidiIn::State midiState{};

	/// Translator from MIDI message bytes to complete message frames.
	MidiIn::MessageStream messageStream{};

	/// Which <dfn>cable</dfn> message stream to monitor for MIDI messages.
	static constexpr MidiIn::UsbMidiStream::CableId cableId{0U};

	/// Translator from USB-MIDI event to MIDI message bytes.
	MidiIn::UsbMidiStream usbMidiStream{
		messageStream, cableId};

	/// USB-MIDI input stream, which puts received messages into midiState
	Project::UsbMidiDevice usbMidiDevice{
		midiState, usbMidiStream};


	/// Stereo sound buffer of samples to write to PWM outputs.
	Project::SoundBuffer soundBuffer{};

	/// Stereo speaker output using pulse-width modulation (PWM).
	Project::Speakers speakers{
		PB_10, PB_15,
		soundBuffer};

	/// Indicator LED that comes on when a note is held on channel one.
	DigitalOut led{LED1};
}




/// Entry point of the Synthesizer firmware.
int main()
{
	puts("____");
	puts("CSUSM CS435 Synthesizer");


	// Setup
	puts("Initializing...");

	::messageStream.messageCompleted_.registerObserver(::midiState);

	::usbMidiDevice.start();
	::soundBuffer.start();
	::speakers.start();


	// Generate sound waves forever
	puts("Synthesizing...");

	while (true) {
		// Turn on LED if playing a note
		const bool notePlaying{
			::midiState.getChannel(0U)->getNotes().getSize() > 0U};
		::led = notePlaying;

		::soundBuffer.refill(midiState);
	}
}
