/// @file
/// The ::Project::Uncopyable class for preventing copy constructors and assignment.
///
/// @copyright Copyright © 2014-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef PROJECT_UNCOPYABLE_HPP_
#define PROJECT_UNCOPYABLE_HPP_


/// An abstract base class that prevents copy constructors and assignment to inheriting instances.
class Uncopyable
{


	/// This protected constructor serves to prevent directly instantiating an Uncopyable.
protected:
	Uncopyable()
	{
	}


	/// Copy constructor disabled for this Uncopyable class.
private:
	Uncopyable(
		const Uncopyable &other);
	// Not defined, and never used


	/// Copy assignment disabled for this Uncopyable class.
private:
	Uncopyable &operator=(
		const Uncopyable &other);
	// Not defined, and never used
};


#endif  // !defined(PROJECT_UNCOPYABLE_HPP_)
