/// @file
/// ::Project::UsbMidiDevice class for acting as a USB MIDI destination port.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef PROJECT_USBMIDIDEVICE_HPP_
#define PROJECT_USBMIDIDEVICE_HPP_


#include <cstdint>

#include "mbed/mbed.h"

#include "MidiIn/State.hpp"
#include "MidiIn/UsbMidiStream.hpp"

#include "USBDevice/USBDevice/USBDevice.h"
#include "USBDevice/USBMIDI/MIDIMessage.h"


namespace Project
{
	/// Hardware abstraction layer responsible for the USB MIDI interface.
	///   This defines how the USB device identifies itself to host computers when
	///   connected, including name, description, and the number and types of MIDI
	///   ports offered.
	class UsbMidiDevice :
		private USBDevice
	{


		/// Running state of the MIDI device to update with received messages.
	private:
		MidiIn::State &midiState_;

		/// Translates USB-MIDI bulk endpoint events into a MIDI message stream.
	private:
		MidiIn::UsbMidiStream &usbMidiStream_;




		/// Initializes a USB-MIDI device to read incoming MIDI messages.
		/// @param[in] midiState  MIDI state data that receives all MIDI messages.
		/// @param[in] vendorId  Registered USB device vendor ID.
		/// @param[in] productId  Registered USB device product ID.
		/// @param[in] productRelease  Version number of USB device.
	public:
		UsbMidiDevice(
			MidiIn::State &midiState,
			MidiIn::UsbMidiStream &usbMidiStream,
			const std::uint16_t vendorId       = 0x16C0,  // Van Ooijen Technische Informatica
			const std::uint16_t productId      = 0x05E4,  // Free shared USB VID/PID pair for MIDI devices
			const std::uint16_t productRelease = 0xC435);  // CSUSM CS435




		/// Receives four-byte USB-MIDI events and translates them into MIDI message bytes.
	protected:
		virtual bool EPBULK_OUT_callback();




		/// Defines the USB interface, including MIDI jacks.
		/// @warn Called in ISR context.
		/// @returns `false` if @p configuration is not supported.
	protected:
		virtual bool USBCallback_setConfiguration(
			std::uint8_t configuration);




		/// @returns This USB device's audio interface name descriptor string data.
	protected:
		virtual std::uint8_t *stringIinterfaceDesc();




		/// @returns This USB device's manufacturer descriptor string data.
	protected:
		virtual std::uint8_t *stringImanufacturerDesc();




		/// @returns This USB device's product descriptor string data.
	protected:
		virtual std::uint8_t *stringIproductDesc();




		/// @returns This USB device's configuration descriptor data, defining
		///   which interfaces it offers.
	protected:
		virtual std::uint8_t *configurationDesc();




		/// Begins receiving input MIDI messages and adding them to #midiState_.
	public:
		void start();
	};
}


#endif  // !defined(PROJECT_USBMIDIDEVICE_HPP_)
