/// @file
/// ::Project::SoundBuffer class for buffering soundwave output values.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef PROJECT_SOUNDBUFFER_HPP_
#define PROJECT_SOUNDBUFFER_HPP_


#include <cstddef>

#include "mbed/mbed.h"

#include "MidiIn/State.hpp"


namespace Project
{
	/// Buffers soundwave values to be output at a fixed frequency.
	class SoundBuffer
	{
		/// Sampling frequency between samples within SoundBuffer%s.
	public:
		static constexpr float sampleFrequency_{22000.0f};

		/// Inverval between SoundBuffer samples in seconds.
	public:
		static constexpr float samplePeriod_s_{1.0f / sampleFrequency_};

		/// How many seconds of sound that can fit within a SoundBuffer.
	public:
		static constexpr float bufferDuration_s_{0.10f};

		/// Total sample count within a SoundBuffer.
	public:
		static constexpr std::size_t bufferLength_{static_cast<std::size_t>(
			bufferDuration_s_ * sampleFrequency_ + 0.5f)};

		/// Refill SoundBuffer arrays when they're at most this full.
	public:
		static constexpr float refillThreshold_{0.5f};

		/// How many seconds to wait between SoundBuffer refills.
	public:
		static constexpr float refillPeriod_s_{bufferDuration_s_ * refillThreshold_};




		/// Index of the next sample to write into this SoundBuffer.
	private:
		volatile std::size_t bufferWriteIndex_;

		/// Index of the next sample to be read out of this SoundBuffer.
		///   Buffer is empty when this equals #bufferWriteIndex_.
	private:
		volatile std::size_t bufferReadIndex_;

		/// Flag periodically set to `true` indicating the need to call #refill() ASAP.
	private:
		volatile bool refillNeeded_;

		/// Periodically calls #onRefillNeeded() once started with #start().
	private:
		Ticker refillTicker_;


		/// Array of buffered sound samples between `0.0` and `1.0` to output to the left speaker.
	private:
		float bufferLeft_[bufferLength_];

		/// Array of buffered sound samples between `0.0` and `1.0` to output to the right speaker.
	private:
		float bufferRight_[bufferLength_];




	public:
		SoundBuffer();




		/// @returns `true` if this SoundBuffer has no unread samples.
	public:
		inline bool isEmpty() const
		{
			return bufferReadIndex_ == bufferWriteIndex_;
		}




		/// Inserts a stereo sound sample into this SoundBuffer without checking for room.
		///   This increments the #bufferWriteIndex_ for the next call.
		/// @param[in] sampleLeft  Left sound output sample in the range `[0, 1]`.
		/// @param[in] sampleRight  Right sound output sample in the range `[0, 1]`.
	public:
		void writeSample(
			const float sampleLeft,
			const float sampleRight);




		/// Reads a sample from this SoundBuffer without checking if it's not empty.
		///   This increments the #bufferReadIndex_ for the next call.
		/// @param[out] sampleLeft  Receives the next left sound output sample in the
		///   range `[0, 1]`.
		/// @param[out] sampleRight  Receives the next right sound output sample in
		///   the range `[0, 1]`.
	public:
		void readSample(
			float &sampleLeft,
			float &sampleRight);




		/// @returns How many written yet unread samples are left in this SoundBuffer.
	public:
		std::size_t countUnreadSamples() const;




		/// Begins periodically allowing #refill() to be called.
	public:
		void start();




		/// Interrupt called periodically to indicate that this SoundBuffer should be refilled.
	private:
		void onRefillNeeded();




		/// Fills this SoundBuffer with future sound samples.
		/// @param[in] midiState  The state of playing notes on MIDI channels to
		///   refill this buffer with.
		/// @returns `true` if this buffer was refilled.
	public:
		bool refill(
			const MidiIn::State &midiState);
	};
}


#endif  // !defined(PROJECT_SOUNDBUFFER_HPP_)
