/// @file
/// ::MidiIn::State class for tracking MIDI input.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "State.hpp"


namespace MidiIn
{
	/// Initializes the MIDI state to a power-up default of no ::MidiIn:Note%s on.
	State::State()
	{
		reset();
	}




	/// Resets the MIDI state to its power-up defaults.
	void State::reset()
	{
		// Reset all channels
		for (ChannelIndex index{0U}; index < numChannels_; ++index) {
			channels_[index].reset();
		}
	}




		/// Receives and handles new messages from a ::MidiIn::MessageStream.
		/// @param[in] message  The complete MIDI message to handle.
	void State::notify(
		const MessageStream::MessageCompleted &message)
	{
		handleMessage(
			message.getStatusType(), message.getStatusChannel(),
			message.getData(), message.getDataLength());
	}




	/// Handles the given raw MIDI message, dispatching it to ::MidiIn::Channel%s as necessary.
	/// @param[in] type  MIDI message status type.
	/// @param[in] channel  MIDI channel value the message is addressed to.
	/// @param[in] data  Array of message data bytes, with length @p dataLength.
	/// @param[in] dataLength  Number of message data bytes within @p data.
	void State::handleMessage(
		const StatusIndex type,
		const ChannelIndex channel,
		const MessageData * const data,
		const std::size_t dataLength)
	{
		if (type == MessageTypes::System) {
			switch (channel) {
			case SystemMessageTypes::Reset:  // Return to power-up state
				reset();
				break;
			}

		} else if (channel < numChannels_) {  // Channel message
			// Dispatch to channel
			channels_[channel].handleMessage(type, data, dataLength);
		}
	}
}
