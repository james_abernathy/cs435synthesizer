/// @file
/// ::MidiIn::UsbMidiStream class that transcodes 4-byte USB-MIDI messages to a ::MidiIn::MessageStream.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "UsbMidiStream.hpp"

#include "mbed/platform/mbed_assert.h"


namespace MidiIn
{
	UsbMidiStream::UsbMidiStream(
		MessageStream &messageStream,
		const CableId cableId
	) :
		cableId_{cableId},
		messageStream_{messageStream}
	{
	}




	void UsbMidiStream::parseMessageData(
		const std::uint8_t *messageDataBuffer)
	{
		static constexpr std::uint8_t codeIndexBits{4U};
		static constexpr std::uint8_t codeIndexCount{1U << codeIndexBits};
		static constexpr std::uint8_t codeIndexMask{codeIndexCount - 1U};

		// Table of "Code Index Number" message lengths, taken from Universal
		//   Serial Bus Device Class Definition for MIDI Devices, Release 1.0.
		static constexpr std::uint8_t codeIndexLengths[codeIndexCount] = {
			0U,  // Miscellaneous function codes. Reserved for future extensions.
			0U,  // Cable events. Reserved for future expansion.
			2U,  // Two-byte System Common messages like MTC, SongSelect, etc.
			3U,  // Three-byte System Common messages like SPP, etc.
			3U,  // SysEx starts or continues.
			1U,  // Single-byte System Common Message or SysEx ends with following single byte/
			2U,  // SysEx ends with following two bytes.
			3U,  // SysEx ends with following three bytes.
			3U,  // Note-off.
			3U,  // Note-on.
			3U,  // Poly-KeyPress.
			3U,  // Control Change.
			2U,  // Program Change.
			2U,  // Channel Pressure.
			3U,  // PitchBend Change.
			1U   // Single Byte
		};

		const std::uint8_t eventByte{*(messageDataBuffer++)};

		const CableId cableId{static_cast<CableId>(
			eventByte >> codeIndexBits)};
		const std::uint8_t codeIndex{static_cast<std::uint8_t>(
			eventByte & codeIndexMask)};

		if (cableId != cableId_  // Wrong cable
			|| codeIndex >= codeIndexCount)  // Impossible
		{
			return;
		}

		const std::uint8_t codeIndexLength{codeIndexLengths[codeIndex]};
		MBED_ASSERT(codeIndexLength <= eventLength_ - 1U);

		for (std::uint_fast8_t remaining{codeIndexLength}; remaining > 0U; --remaining) {
			messageStream_.parseByte(*(messageDataBuffer++));
		}
	}
}
