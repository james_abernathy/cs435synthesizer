/// @file
/// ::MidiIn::Note class, representing a single MIDI note.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_NOTE_HPP_
#define MIDIIN_NOTE_HPP_


#include "Types.hpp"


namespace MidiIn
{
	/// A single MIDI note at a given semitone, volume, etc.
	///   Most properties of notes don't change after construction, so few setters
	///   are exposed. Because ::MidiIn::NoteList%s allocate space for many
	///   concurrent notes per ::MidiIn::Channel, eliminating unnecessary
	///   properties can save quite a bit of memory. A number of preprocessor
	///   flags control which member variables and methods to exclude for the
	///   sake of memory.
	class Note
	{


		friend class NoteList;




		/// Sentinel value for the #pressure_ member, for inheriting pressure from the containing ::MidiIn::Channel.
	public:
		static const MessageData pressureInherited_;




		/// This Note's pitch as a MIDI semitone.
		///   Values range between `0` and ::MidiIn::maxMessageDataValue for max pitch.
	private:
		MessageData semitone_;


		/// The initial attack velocity for this note.
		///   Values between `0` for off and ::MidiIn::maxMessageDataValue for max velocity.
	private:
		MessageData attack_;


		/// @def MIDIIN_NOTE_NO_PRESSURE
		/// Reduces the size of ::MidiIn::Note%s by omitting their ::MidiIn::Note::pressure_ values.
		///   If memory is scarce, this can save about 256 bytes per ::MidiIn::State.
#ifndef MIDIIN_NOTE_NO_PRESSURE

		/// This Note's after-touch pressure.
		///   Values range between `0` and ::MidiIn::maxMessageDataValue for max
		///   pressure. The additional sentinel value #pressureInherited_ indicates
		///   that this Note inherits its pressure from the containing
		///   ::MidiIn::Channel.
		/// @note This member is only available if ::MIDIIN_NOTE_NO_PRESSURE isn't defined.
	private:
		MessageData pressure_;
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)


		/// @def MIDIIN_NOTE_NO_STARTTIME
		/// Reduces the size of ::MidiIn::Note%s by omitting their ::MidiIn::Note::startTime_ values.
		///   If memory is scarce, this can save about 1024 bytes per ::MidiIn::State.
#ifndef MIDIIN_NOTE_NO_STARTTIME

		/// The time in milliseconds when this Note started playing, used to find how old it is.
		/// @note This member is only available if ::MIDIIN_NOTE_NO_STARTTIME isn't defined.
	private:
		Time_ms startTime_;
#endif  // !defined(MIDIIN_NOTE_NO_STARTTIME)




		/// Puts this Note into a silent, default state.
		///   Clearing doesn't affect this Note's #startTime_.
	private:
		void clear()
		{
			semitone_ = 0U;  // Low default value allows quickly exiting from searches within NoteList
			attack_ = 0U;  // No attack velocity indicates silence

#ifndef MIDIIN_NOTE_NO_PRESSURE
			setPressure(pressureInherited_);
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)

			// startTime_ is left as-is, since all times are valid
		}




		/// Default assignment operator, for use only by ::MidiIn::NoteList%s when moving Note instances.
		///   ::MidiIn::NoteList%s keep a fixed array of Notes to avoid heap
		///   allocations, so movements are made by assigning instances to one
		///   another. Assignment is protected from other locations to prevent
		///   overwriting "pseudo-constant" members without setters.
	private:
		Note &operator=(
			const Note &other)
		{
			semitone_ = other.semitone_;
			attack_ = other.attack_;

#ifndef MIDIIN_NOTE_NO_PRESSURE
			pressure_ = other.pressure_;
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)

#ifndef MIDIIN_NOTE_NO_STARTTIME
			startTime_ = other.startTime_;
#endif  // !defined(MIDIIN_NOTE_NO_STARTTIME)

			return *this;
		}




		/// Initializes undefined Notes as silent, i.e. with no #attack_ velocity.
	public:
		Note()
#ifndef MIDIIN_NOTE_NO_STARTTIME
		// Placeholder time, since calling ::millis() in constructors is unsafe,
		// such as for global/static variables before Arduino's setup().
		:
			startTime_{0U}
#endif  // !defined(MIDIIN_NOTE_NO_STARTTIME)
		{
			clear();
		}




		/// Constructs a Note at a given @p semitone and @p pressure.
		/// @param[in] semitone  MIDI semitone index, between `0` and
		///   ::MidiIn::maxMessageDataValue.
		/// @param[in] attack  Attack velocity, between `0` and
		///   ::MidiIn::maxMessageDataValue.
		/// @param[in] pressure  Note after-touch pressure, between `0` (off) and
		///   ::MidiIn::maxMessageDataValue (max pressure). This argument is only
		///   available when ::MIDIIN_NOTE_NO_PRESSURE is undefined. A value of
		///   #pressureInherited_ causes this Note to inherit pressure from its
		///   containing ::MidiIn::Channel.
		/// @param[in] startTime  When this Note started playing, in milliseconds.
		///   This argument is only available when ::MIDIIN_NOTE_NO_STARTTIME is undefined.
	public:
		Note(
			const MessageData semitone,
			const MessageData attack

#ifndef MIDIIN_NOTE_NO_PRESSURE
			, const MessageData pressure
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)

#ifndef MIDIIN_NOTE_NO_STARTTIME
			, const Time_ms startTime
#endif  // !defined(MIDIIN_NOTE_NO_STARTTIME)
			)
		{
#ifndef MIDIIN_NOTE_NO_STARTTIME
			// Assign start time whether valid or not
			startTime_ = startTime;
#endif  // !defined(MIDIIN_NOTE_NO_STARTTIME)

			if (semitone > maxMessageDataValue || attack > maxMessageDataValue) {
				// Invalid values
				clear();
				return;
			}

			semitone_ = semitone;
			attack_ = attack;
#ifndef MIDIIN_NOTE_NO_PRESSURE
			setPressure(pressure);
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)
		}




		/// Default copy constructor as an alternative to the private copy assignment.
	public:
		Note(
			const Note &other) = default;




		/// @returns This Note's MIDI semitone index.
		/// @see #semitone_
	public:
		MessageData getSemitone() const
		{
			return semitone_;
		}




		/// @returns This Note's attack velocity.
		/// @see #attack_
	public:
		MessageData getAttack() const
		{
			return attack_;
		}




		/// @returns `true` if this Note is inaudible.
	public:
		bool isSilent() const
		{
			return attack_ == 0U;
		}




#ifndef MIDIIN_NOTE_NO_PRESSURE
		/// @returns This Note's explicit after-touch pressure.
		/// @see #pressure_
		/// @note This method is only available when ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	public:
		MessageData getPressure() const
		{
			return pressure_;
		}




		/// Sets this Note's explicit after-touch pressure.
		/// @see #pressure_
		/// @param[in]  pressure The after-touch pressure to use.
		///   Invalid pressures default to #pressureInherited_.
		/// @note This method is only available when ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	public:
		void setPressure(
			const MessageData pressure)
		{
			if (pressure == pressureInherited_  // Inherit from Channel
				|| pressure <= maxMessageDataValue)  // Valid pressure
			{
				pressure_ = pressure;

			} else {  // Invalid
				pressure_ = pressureInherited_;
			}
		}
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)




#ifndef MIDIIN_NOTE_NO_STARTTIME
		/// Gets the age of this Note in milliseconds.
		/// @param[in]  currentMillis The current time in milliseconds, for comparison.
		/// @returns How many milliseconds this Note has played for.
		/// @note This method is only available when ::MIDIIN_NOTE_NO_STARTTIME is undefined.
		/// @see #startTime_
	public:
		Time_ms getAge(
			const Time_ms currentMillis) const
		{
			return currentMillis - startTime_;
		}
#endif  // !defined(MIDIIN_NOTE_NO_STARTTIME)
	};
}


#endif  // !defined(MIDIIN_NOTE_HPP_)
