/// @file
/// ::MidiIn::Channel class, which keeps track of active notes and channel-wide properties.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "Channel.hpp"

#include <limits>


namespace MidiIn
{
	/// Initializes this Channel to an empty state.
	Channel::Channel()
	{
		reset();
	}




	/// Returns this Channel to its start-up state by clearing active ::MidiIn::Notes and properties.
	void Channel::reset()
	{
		resetNotes();
		resetControllers();

#ifndef MIDIIN_CHANNEL_NO_PROGRAM
		// Reset program
		bankPending_ = 0U;
		messageProgramChange(0U);
#endif  // !defined(MIDIIN_CHANNEL_NO_PROGRAM)

		parameterRegistered_ = false;
		parameter_ = RegisteredParameterTypes::None;
	}




	/// Disables any playing ::MidiIn::Notes on this Channel.
	void Channel::resetNotes()
	{
		notes_.clear();
		notesCleared_.notifyObservers();
	}




	/// Resets all controllers for this Channel.
	void Channel::resetControllers()
	{
		setPitchBend(pitchBendNeutral_, pitchBendRangeDefault_);
		setVolumeEffective(volumeDefault_, expressionDefault_);

#ifndef MIDIIN_NOTE_NO_PRESSURE
		messageChannelPressure(pressureDefault_);
		// Also remove all explicit after-touch pressures
		for (NoteList::Index index{0U}; index < notes_.getSize(); ++index) {
			messageAfterTouchByIndex(index, Note::pressureInherited_);
		}
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)
	}




	/// Handles ::MidiIn::MessageTypes::Enum::NoteOn by adding a new ::MidiIn::Note to this Channel.
	/// @param[in] semitone  The new ::MidiIn::Note's semitone.
	/// @param[in] attack  The attack velocity to start the new ::MidiIn::Note.
	void Channel::messageNoteOn(
		const MessageData semitone,
		const MessageData attack)
	{
		Note newNote(semitone, attack
#ifndef MIDIIN_NOTE_NO_PRESSURE
			, Note::pressureInherited_
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)
#ifndef MIDIIN_NOTE_NO_STARTTIME
			, 0U  // TODO: Pass current RTC time (ms) in from State top level.
#endif  // !defined(MIDIIN_NOTE_NO_STARTTIME)
			);

		// Make sure this note isn't already playing
		messageNoteOff(semitone, releaseDefault_);
		if (newNote.isSilent()) {
			return;  // Don't re-add
		}

		// Add new note
		NoteList::Index index{notes_.add(newNote)};
		if (index < notes_.getSize()) {  // Success
			noteAdded_.notifyObservers(index);
		}
	}




	/// Handles ::MidiIn::MessageTypes::Enum::NoteOff by removing a playing ::MidiIn::Note from this Channel.
	/// @param[in] semitone  Which ::MidiIn::Note semitone to remove.
	/// @param[in] release  The release velocity to stop the ::MidiIn::Note with.
	void Channel::messageNoteOff(
		const MessageData semitone,
		const MessageData release)
	{
		if (release > maxMessageDataValue) {  // Invalid
			return;  // Ignore
		}

		NoteList::Index index{notes_.findIndexBySemitone(semitone)};
		if (index < notes_.getSize()) {  // Was playing
			// Copy note for notification before removing
			const Note removed{*notes_.getNote(index)};
			notes_.remove(index);
			noteRemoved_.notifyObservers(removed, release);
		}
	}




	/// Handles ::MidiIn::MessageTypes::Enum::PitchWheel, bending all of this Channel's ::MidiIn::Note%s.
	///   Both @p bendLow and @p bendHigh are combined into a 14-bit bend amount,
	///   relative to a central neutral value of `0x2000`. Values above neutral
	///   raise pitch. The actual bend amount in cents depends upon #pitchBendRange_.
	/// @param[in] bendLow  Bits 0-6 of the 14-bit pitch bend amount.
	/// @param[in] bendHigh  Bits 7-13 of the 14-bit pitch bend amount.
	void Channel::messagePitchWheel(
		const MessageData bendLow,
		const MessageData bendHigh)
	{
		if (bendLow > maxMessageDataValue || bendHigh > maxMessageDataValue) {  // Invalid
			return;  // Ignore
		}

		setPitchBend(joinContinuous(bendHigh, bendLow), pitchBendRange_);
	}
	



	/// Re-calculates and caches this Channel's pitch bend in cents.
	/// @param[in] pitchBend  New 14-bit bend value to apply.
	/// @param[in] pitchBendRange  New min/max pitch bend magnitude in cents.
	void Channel::setPitchBend(
		const Continuous pitchBend,
		const Cents pitchBendRange)
	{
		if (pitchBend_ == pitchBend
			&& pitchBendRange_ == pitchBendRange)
		{
			return;  // No change
		}

		const Cents pitchBendCentsOld{getPitchBendCents()};
		pitchBend_ = pitchBend;
		pitchBendRange_ = pitchBendRange;

		// Bend value, within [-pitchBendNeutral_, pitchBendNeutral_).
		{
			const Cents bendRelative{static_cast<Cents>(
				static_cast<Cents>(pitchBend) - static_cast<Cents>(pitchBendNeutral_))};
			const std::int32_t product{
				static_cast<std::int32_t>(pitchBendRange) * bendRelative};

			// Round similar to `floor(value + 0.5)` technique before dividing
			pitchBendCents_ = static_cast<Cents>(
				(product + pitchBendNeutral_ / 2) / pitchBendNeutral_);
		}

		if (pitchBendCentsOld != getPitchBendCents()) {
			propertyChanged_.notifyObservers();
		}
	}




#ifndef MIDIIN_CHANNEL_NO_PROGRAM
	/// Changes this Channel's program, or instrument, within the pending bank.
	/// @param[in] program  The program to use within the pending bank.
	/// @note This method is only available if ::MIDIIN_CHANNEL_NO_PROGRAM is undefined.
	void Channel::messageProgramChange(
		const MessageData program)
	{
		if ((bank_ == bankPending_ && program_ == program)  // No change
			|| program > maxMessageDataValue)  // Invalid
		{
			return;  // Ignore
		}

		// Store new bank and program
		bank_ = bankPending_;
		program_ = program;

		propertyChanged_.notifyObservers();
	}
#endif  // !defined(MIDIIN_CHANNEL_NO_PROGRAM)




#ifndef MIDIIN_NOTE_NO_PRESSURE
	/// Handles ::MidiIn::MessageTypes::Enum::AfterTouch which updates a *single* ::MidiIn::Note's pressure.
	/// @param[in] index  The index to modify within #notes_.
	/// @param[in] notePressure  The new after-touch pressure to apply.
	/// @note This method is only available if ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	void Channel::messageAfterTouchByIndex(
		const NoteList::Index index,
		const MessageData notePressure)
	{
		Note *affectedNote{notes_.getNote(index)};
		if (notePressure == affectedNote->getPressure()) {  // No change
			return;
		}

		const Volume effectiveNotePressure{getEffectiveNotePressure(index)};
		// Assign explicit note pressure
		affectedNote->setPressure(notePressure);
		if (effectiveNotePressure != getEffectiveNotePressure(index)) {
			notePressureChanged_.notifyObservers(index);
		}
	}




	/// Handles ::MidiIn::MessageTypes::Enum::AfterTouch by updating a ::MidiIn::Note's pressure.
	/// @param[in] semitone  Which ::MidiIn::Note semitone to modify.
	/// @param[in] notePressure  The new after-touch pressure to apply.
	/// @note This method is only available if ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	void Channel::messageAfterTouch(
		const MessageData semitone,
		const MessageData notePressure)
	{
		const NoteList::Index index{notes_.findIndexBySemitone(semitone)};
		if (index < notes_.getSize()) {  // Note was already playing
			messageAfterTouchByIndex(index, notePressure);
		}
	}




	/// Handles ::MidiIn::MessageTypes::Enum::ChannelPressure by updating this Channel's pressure.
	/// @param[in] pressure  The pressure for ::MidiIn::Note%s without after-touch
	///   to inherit.
	/// @note This method is only available if ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	void Channel::messageChannelPressure(
		const MessageData pressure)
	{
		if (pressure_ == pressure  // No change
			|| pressure > maxMessageDataValue)  // Invalid
		{
			return;
		}

		pressure_ = pressure;
		// Notify all notes without explicit after-touch pressures
		for (NoteList::Index index{0U}; index < notes_.getSize(); ++index) {
			const MessageData notePressure{notes_.getNote(index)->getPressure()};
			if (notePressure == Note::pressureInherited_) {  // Was affected
				notePressureChanged_.notifyObservers(index);
			}
		}
	}
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)




	/// Re-calculates this Channel's effective volume returned by #getVolume().
	/// @param[in] volume  New volume level to mix with @p expression, between
	///   `0` and ::MidiIn::maxMessageDataValue.
	/// @param[in] expression  New expression level to mix with @p volume, between
	///   `0` and ::MidiIn::maxMessageDataValue.
	void Channel::setVolumeEffective(
		const MessageData volume,
		const MessageData expression)
	{
		if (volume_ == volume && expression_ == expression) {  // No change
			return;
		}

		const Volume volumeEffectiveOld{getVolume()};
		volume_ = volume;
		expression_ = expression;

		// Cache new effective volume
		{
			const Continuous volumeAndExpression{static_cast<Continuous>(volume_ * expression_)};

			volumeEffective_ = static_cast<Volume>(volumeAndExpression) / maxContinuousValue;
		}

		if (volumeEffectiveOld != getVolume()) {
			propertyChanged_.notifyObservers();
		}
	}




	/// Handles all types of ::MidiIn::MessageTypes::Enum::Controller messages.
	/// @param[in] controller  The MIDI controller ID that @p data modifies,
	///   between `0` and ::MidiIn::maxMessageDataValue.
	/// @param[in] data  The new value of @p controller, between `0` and
	///   ::MidiIn::maxMessageDataValue.
	void Channel::messageController(
		const MessageData controller,
		const MessageData data)
	{
		if (controller > maxMessageDataValue || data > maxMessageDataValue) {  // Invalid
			return;
		}
		switch (controller) {
			case ControllerTypes::VolumeCoarse: {
				// Channel volume MSB (fine control ignored since such precision isn't audible)
				setVolumeEffective(data, expression_);
				break;
			}

			case ControllerTypes::ExpressionCoarse: {
				// Channel expression MSB (fine control ignored since such precision isn't audible)
				setVolumeEffective(volume_, data);
				break;
			}

#ifndef MIDIIN_CHANNEL_NO_PROGRAM
			case ControllerTypes::BankSelectFine: {
				// Program bank LSB
				bankPending_ = setContinuousFine(bankPending_, data);  // Used when program changes
				break;
			}

			case ControllerTypes::BankSelectCoarse: {
				// Program bank MSB
				bankPending_ = setContinuousCoarse(bankPending_, data);  // Used when program changes
				break;
			}
#endif  // !defined(MIDIIN_CHANNEL_NO_PROGRAM)

			case ControllerTypes::RegisteredParameterCoarse: {
				// RPN MSB
				parameterRegistered_ = true;
				parameter_ = setContinuousCoarse(parameter_, data);
				break;
			}

			case ControllerTypes::RegisteredParameterFine: {
				// RPN LSB
				parameterRegistered_ = true;
				parameter_ = setContinuousFine(parameter_, data);
				break;
			}

			case ControllerTypes::NonRegisteredParameterCoarse: {
				// NRPN MSB
				parameterRegistered_ = false;
				parameter_ = setContinuousCoarse(parameter_, data);
				break;
			}

			case ControllerTypes::NonRegisteredParameterFine: {
				// NRPN LSB
				parameterRegistered_ = false;
				parameter_ = setContinuousFine(parameter_, data);
				break;
			}

			case ControllerTypes::DataEntryCoarse: {
				// MSB of data entry for preceding [N]RPN
				Continuous value;
				if (getParameterValue(value)) {
					setParameterValue(setContinuousCoarse(value, data));
				}
				break;
			}

			case ControllerTypes::DataEntryFine: {
				// LSB of data entry for preceding [N]RPN
				Continuous value;
				if (getParameterValue(value)) {
					setParameterValue(setContinuousFine(value, data));
				}
				break;
			}

			case ControllerTypes::DataButtonIncrement: {
				// Increment preceding [N]RPN
				Continuous value;
				if (getParameterValue(value) && value < maxContinuousValue) {
					// Won't overflow
					setParameterValue(value + 1U);
				}
				break;
			}

			case ControllerTypes::DataButtonDecrement: {
				// Decrement preceding [N]RPN
				Continuous value;
				if (getParameterValue(value) && value > 0U) {
					// Won't underflow
					setParameterValue(value - 1U);
				}
				break;
			}

			case ControllerTypes::AllSoundsOff:
			case ControllerTypes::AllNotesOff: {
				// Disable all playing notes
				resetNotes();
				break;
			}

			case ControllerTypes::AllControllersOff: {
				// Reset all controllers and similar states
				resetControllers();
				break;
			}
		}
	}




	/// Gets the current registered/unregistered parameter's continuous data.
	/// @param[out] value  The current parameter's value gets written here if known.
	/// @returns `true` if the current parameter is known, indicating that
	///   @p value was assigned.
	bool Channel::getParameterValue(
		Continuous &value) const
	{
		if (parameterRegistered_) {
			switch (parameter_) {
				case RegisteredParameterTypes::None: {
					break;
				}

				case RegisteredParameterTypes::PitchBendSensitivity: {
					// Convert from raw cents to parameter's semitone-cents combo
					value = getPitchBendRangeParameter(pitchBendRange_);
					return true;
				}
			}
		}

		return false;  // Parameter isn't tracked
	}




	/// Sets the current registered/unregistered parameter's continuous data.
	/// @param[in] value  A new value for the current [Non-]Registered Parameter
	///   Number ([N]RPN), #parameter_.
	void Channel::setParameterValue(
		const Continuous value)
	{
		if (parameterRegistered_) {
			switch (parameter_) {
				case RegisteredParameterTypes::None: {
					break;
				}

				case RegisteredParameterTypes::PitchBendSensitivity: {
					// Convert from parameter's semitone-cents combo to raw cents
					setPitchBend(pitchBend_, getPitchBendRangeCents(value));
					break;
				}
			}
		}
	}




	/// Converts a pitch bend range to a MidiIn::Continuous parameter value.
	/// @param[in] centsTotal  The total bend range in cents.
	/// @returns The pitch bend sensitivity Registered Parameter Number (RPN)'s
	///   value, including high and low 7-bit halves which are used independently
	///   to represent the combined semitones plus cents range, respectively.
	/// @see The inverse of this function is #getPitchBendRangeCents().
	Continuous Channel::getPitchBendRangeParameter(
		const Cents centsTotal)
	{
		const MessageData semitones{static_cast<MessageData>(
			centsTotal / centsPerSemitone)};
		const MessageData cents{static_cast<MessageData>(
			centsTotal % centsPerSemitone)};

		return joinContinuous(semitones, cents);
	}




	/// Converts a ::MidiIn::Continuous pitch bend sensitivity into the total cent ramge.
	/// @param[in] parameter  The pitch bend sensitivity parameter's value,
	///   including high and low 7-bit halves which are used independently to
	///   represent the combined semitones plus cents range, respectively.
	/// @returns The total bend range in cents.
	/// @see The inverse of this function is #getPitchBendRangeParameter().
	Cents Channel::getPitchBendRangeCents(
		const Continuous parameter)
	{
		const MessageData semitones{getContinuousCoarse(parameter)};
		const MessageData cents{getContinuousFine(parameter)};

		return static_cast<Cents>(semitones) * centsPerSemitone + cents;
	}




	/// Handles the given channel-specific MIDI message.
	/// @param[in] type  MIDI message status type.
	/// @param[in] data  Address of message data bytes, with length @p dataLength.
	/// @param[in] dataLength  Number of message data bytes within @p data.
	void Channel::handleMessage(
		const StatusIndex type,
		const MessageData * const data,
		const std::size_t dataLength)
	{
		switch (type) {
			case MessageTypes::NoteOn: {
				// Add new note
				if (dataLength >= 2U) {  // A semitone and attack velocity
					messageNoteOn(data[0U], data[1U]);
				}
				break;
			}

			case MessageTypes::NoteOff: {
				// Remove existing note
				if (dataLength >= 2U) {  // A semitone and release velocity
					messageNoteOff(data[0U], data[1U]);
				}
				break;
			}

			case MessageTypes::PitchWheel: {
				// Bend pitch of notes on this channel
				if (dataLength >= 2U) {  // Low and high bits
					messagePitchWheel(data[0U], data[1U]);
				}
				break;
			}

#ifndef MIDIIN_NOTE_NO_PRESSURE
			case MessageTypes::AfterTouch: {
				// Update note pressure
				if (dataLength >= 2U) {  // A semitone and new pressure
					messageAfterTouch(data[0U], data[1U]);
				}
				break;
			}

			case MessageTypes::ChannelPressure: {
				// Update all note pressures at once
				if (dataLength >= 1U) {  // Channel's new pressure
					messageChannelPressure(data[0U]);
				}
				break;
			}
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)

			case MessageTypes::Controller: {
				if (dataLength >= 2U) {  // An ID and value
					messageController(data[0U], data[1U]);
				}
				break;
			}
		}
	}




	/// Initializes this ::MidiIn::Observable to an unset state before ever having fired.
	Channel::NoteAdded::NoteAdded() :
		index_{0U}
	{
	}




	/// Notifies listening ::Project::Observer%s when a ::MidiIn::Note is added.
	/// @param[in] index  The position within the driving ::MidiIn::Channel's
	///   ::MidiIn::NoteList where the new ::MidiIn::Note was added.
	void Channel::NoteAdded::notifyObservers(
		const NoteList::Index index)
	{
		index_ = index;
		notifyObservers();
	}




	/// Gets the position of the ::MidiIn::Note that was just added.
	/// @returns An index into the driving ::MidiIn::Channel's ::MidiIn::NoteList
	///   where the new ::MidiIn::Note was added.
	NoteList::Index Channel::NoteAdded::getIndex() const
	{
		return index_;
	}




#ifndef MIDIIN_NOTE_NO_PRESSURE
	/// Initializes this ::Project::Observable to an unset state before ever having fired.
	Channel::NotePressureChanged::NotePressureChanged() :
		index_{0U}
	{
	}




	/// Notifies listening ::Project::Observer%s when an existing ::MidiIn::Note's pressure has changed.
	/// @param[in] index  The position within the driving ::MidiIn::Channel's
	///   ::MidiIn::NoteList where the updated ::MidiIn::Note exists.
	void Channel::NotePressureChanged::notifyObservers(
		const NoteList::Index index)
	{
		index_ = index;
		notifyObservers();
	}




	/// Gets the position of the ::MidiIn::Note that just changed pressure.
	/// @returns An index into the driving ::MidiIn::Channel's ::MidiIn::NoteList
	///   where the updated ::MidiIn::Note exists.
	NoteList::Index Channel::NotePressureChanged::getIndex() const
	{
		return index_;
	}
#endif  // !MIDIIN_NOTE_NO_PRESSURE




	/// Initializes this ::Project::Observable to an unset state before ever having fired.
	Channel::NoteRemoved::NoteRemoved() :
		note_{nullptr},
		release_{0U}
	{
	}




	/// Notifies listening ::Project::Observer%s when an existing ::MidiIn::Note was removed.
	/// @param[in] note  A copy of the removed ::MidiIn::Note that is no longer playing.
	/// @param[in] release  The ::MidiIn::Note's release velocity value, indicating
	///   how quickly it should fade.
	void Channel::NoteRemoved::notifyObservers(
		const Note &note,
		const MessageData release)
	{
		note_ = &note;
		release_ = release;
		notifyObservers();
		note_ = nullptr;  // Clear while not notifying
	}




	/// Gets the removed ::MidiIn::Note.
	/// @returns A copy of the ::MidiIn::Note instance that stopped sounding, or
	///   `nullptr` when not notifying.
	const Note *Channel::NoteRemoved::getNote() const
	{
		return note_;
	}




	/// Gets the removed ::MidiIn::Note's release velocity.
	/// @returns The release velocity to stop the removed ::MidiIn::Note with,
	///   between `0` and ::MidiIn::maxMessageDataValue.
	MessageData Channel::NoteRemoved::getRelease() const
	{
		return release_;
	}




	/// Initializes this ::Project::Observable to an unset state before ever having fired.
	Channel::NotesCleared::NotesCleared()
	{
	}




	/// Initializes this ::Project::Observable to an unset state before ever having fired.
	Channel::SoundChanged::SoundChanged()
	{
	}
}
