/// @file
/// A collection of active ::MidiIn::Note%s, ordered by semitone.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_NOTELIST_HPP_
#define MIDIIN_NOTELIST_HPP_


#include "../Uncopyable.hpp"
#include "Types.hpp"
#include "Note.hpp"


namespace MidiIn
{
	/// A collection of active ::MidiIn::Note%s and their properties.
	///   They're stored in sorted order from lowest semitone to highest.
	class NoteList :
		private Uncopyable
	{


		/// Data type of ::MidiIn::Note indexes within a NoteList.
	public:
		typedef std::uint8_t Index;




		/// Max number of simultaneously playing ::MidiIn::Note%s, where extras are discarded.
		///   Because the MIDI standard limits semitone indices between `0` and
		///   ::MidiIn::maxMessageDataValue, only `128` notes can possibly play
		///   simultaneously.
	public:
		static constexpr Index sizeMax_{8U};




		/// The index into #notes_ where the next ::MidiIn::Note will be added.
	private:
		Index next_;

		/// The set of all active ::MidiIn::Note%s within this NoteList.
	private:
		Note notes_[sizeMax_];




	public:
		NoteList();

	private:
		Index findInsertionIndex(
			const MessageData semitone) const;

	public:
		Index findIndexBySemitone(
			const MessageData semitone) const;

	public:
		Index add(
			const Note &note);

	public:
		void remove(
			Index index);

	public:
		bool removeBySemitone(
			const MessageData semitone);




		/// Empties this NoteList of active ::MidiIn::Note%s.
	public:
		void clear()
		{
			next_ = 0U;
		}


		/// Returns the number of active ::MidiIn::Note%s within this NoteList.
		/// @returns How many ::MidiIn::Note%s are still playing.
	public:
		Index getSize() const
		{
			return next_;
		}


		/// Gets the ::MidiIn::Note at a given @p index within this NoteList.
		/// @param[in] index  The index into this sorted set of notes between `0`
		///   and #getSize() - `1`.
		/// @returns The ::MidiIn::Note at @p index, or `nullptr` if that note isn't
		///   playing.
	public:
		Note *getNote(
			const Index index)
		{
			if (index < getSize()) {
				return &notes_[index];
			}
			return nullptr;  // Invalid
		}


		/// @copydoc #getNote(const Index)
	public:
		const Note *getNote(
			const Index index) const
		{
			if (index < getSize()) {
				return &notes_[index];
			}
			return nullptr;  // Invalid
		}
	};
}


#endif  // !defined(MIDIIN_NOTELIST_HPP_)
