/// @file
/// Common constant definitions for the ::MidiIn namespace.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_CONSTANTS_HPP_
#define MIDIIN_CONSTANTS_HPP_


#include "Types.hpp"


namespace MidiIn
{
	/// Number of cents between each semitone/half-step/MIDI note index.
	constexpr Cents centsPerSemitone{100};
}


#endif  // !defined(MIDIIN_CONSTANTS_HPP_)
