@ECHO OFF
:: Runs generate_note_periods.py to recreate NotePeriods.cpp.

python "generate_note_periods.py" "NotePeriods.cpp"
EXIT /B %ERRORLEVEL%
