/// @file
/// ::MidiIn::MessageStream class that interprets MIDI serial input as MIDI messages.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_MESSAGESTREAM_HPP_
#define MIDIIN_MESSAGESTREAM_HPP_


#include "../Uncopyable.hpp"
#include "../Observable.hpp"
#include "Types.hpp"


namespace MidiIn
{
	/// An interpreter for MIDI serial input bytes which assembles them into MIDI messages.
	class MessageStream :
		private Uncopyable
	{


		/// @def MIDIIN_LISTENER_NO_SYSEX_BUFFER
		/// Shrinks the message buffer of ::MidiIn::MessageStream%s as much as possible when defined.
		///   The buffer still gets used for fixed-length message data, but no
		///   additional space is reserved for long SysEx messages.
		/// @note SysEx messages small enough to fit within the minimal buffer still
		///   count as complete messages in #readMessage().




		/// Index of ::MidiIn::MessageData bytes within #data_.
	public:
		typedef std::uint16_t DataIndex;




		/// Sentinel value for #runningStatusType_ when none are running.
	private:
		static const StatusIndex runningStatusNone_;

		/// @var #maxDataBytes_
		/// The size of the data buffer for message data--SysEx variable-length messages in particular.
		///   This must be at least `2` to fit non-SysEx data bytes for normal MIDI
		///   messages.
	private:
		static constexpr DataIndex maxDataBytes_{
#ifdef MIDIIN_LISTENER_NO_SYSEX_BUFFER
			2U  // Minimum required size for non-SysEx data bytes
#else  // !defined(MIDIIN_LISTENER_NO_SYSEX_BUFFER)
			1024U
#endif  // !defined(MIDIIN_LISTENER_NO_SYSEX_BUFFER)
		};

		/// The MIDI standard specifies a fixed baud rate of `31250` bits per second.
	public:
		static constexpr unsigned long baudRateDefault_{31250UL};




		/// Data bytes collected for the current completed non-realtime message.
	private:
		MessageData data_[maxDataBytes_];

		/// The last non-realtime message status type that was received, or `0` when none is active.
		///   Input may omit status bytes of non-system messages when identical
		///   message statuses are used back to back. System common messages reset
		///   this value once complete, and system realtime messages can never be
		///   running because they have no data bytes.
		/// @see Valid values are enumerated in ::MidiIn::MessageTypes::Enum.
	private:
		StatusIndex runningStatusType_;

		/// The last non-realtime message status channel that was received, or `0` when none is active.
		/// @see See #runningStatusType_ for an explanation of running statuses.
		/// @see Valid system message values are enumerated within
		///   ::MidiIn::SystemMessageTypes::Enum.
	private:
		ChannelIndex runningStatusChannel_;

		/// Number of data bytes filled for the message in progress, within #data_.
	private:
		DataIndex runningDataLength_;




	private:
		void parseByteStatus(
			const StatusIndex newStatusType,
			const ChannelIndex newStatusChannel);

	private:
		void parseByteData(
			const MessageData byte);

	private:
		void setCompletedMessage(
			const StatusIndex statusType,
			const ChannelIndex statusChannel,
			const DataIndex dataLength = 0U);

	private:
		void setRunningStatus(
			const StatusIndex statusType,
			const ChannelIndex statusChannel);

	private:
		void clearRunningStatus();

	public:
		MessageStream();

	public:
		void parseByte(
			const std::uint8_t byte);




		/// Observer-pattern subject that notifies ::Project::Observer%s when this MessageStream has received a complete MIDI message.
	public:
		class MessageCompleted :
			public Observable<MessageCompleted>
		{

			friend class MessageStream;


			/// The ::MidiIn::MessageStream tracked by this ::Observable.
		private:
			const MessageStream &messageStream_;


			/// The completed message's high status byte nibble, indicating its status type.
			/// @see Valid values are enumerated in ::MidiIn::MessageTypes::Enum.
		private:
			StatusIndex statusType_;

			/// The completed message's low status byte nibble, indicating its channel number for voice messages, or a status sub-type for system messages.
			/// @see Valid system message values are enumerated within ::MidiIn::SystemMessageTypes::Enum.
		private:
			ChannelIndex statusChannel_;

			/// Number of data bytes filled for the completed message, ::MidiIn::MessageStream::data_.
		private:
			DataIndex dataLength_;


		private:
			using Observable<MessageCompleted>::notifyObservers;

		private:
			void notifyObservers(
				const StatusIndex statusType,
				const ChannelIndex statusChannel,
				const DataIndex dataLength);


		private:
			MessageCompleted(
				const MessageStream &messageStream)
			:
				messageStream_{messageStream},
				statusType_{0U},
				statusChannel_{0U},
				dataLength_{0U}
			{
			}


			/// @returns The high status byte nibble of this completed message.
			/// @see #statusType_
		public:
			StatusIndex getStatusType() const
			{
				return statusType_;
			}


			/// @returns The low status byte nibble of this completed message.
			/// @see #statusChannel_
		public:
			ChannelIndex getStatusChannel() const
			{
				return statusChannel_;
			}


			/// @returns The number of data bytes buffered for this completed message.
			/// @see #dataLength_
		public:
			DataIndex getDataLength() const
			{
				return dataLength_;
			}


			/// @returns The buffer containing this completed message's data.
			/// @see #getDataLength()
			/// @see ::MidiIn::MessageStream::data_
			/// @warning This buffer only remains unchanged during ::Observer notification.
		public:
			const MessageData *getData() const
			{
				return messageStream_.data_;
			}

		} messageCompleted_;  ///< Notifies ::Project::Observer%s when a new MIDI message is ready.
	};
}


#endif  // !defined(MIDIIN_MESSAGESTREAM_HPP_)
