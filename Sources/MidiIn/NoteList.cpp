/// @file
/// A collection of active ::MidiIn::Note%s, ordered by semitone.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "NoteList.hpp"

#include "Types.hpp"
#include "Note.hpp"


namespace MidiIn
{
	/// Initializes this NoteList to an empty set.
	NoteList::NoteList()
	{
		clear();
	}




	/// Finds a valid index to place a new ::MidiIn::Note of @p semitone within this NoteList.
	///   ::MidiIn::Note%s are inserted in sorted order from lowest to highest
	///   semitone.
	/// @param[in] semitone  The MIDI semitone index to find an array index for.
	/// @returns The index where a ::MidiIn::Note of @p semitone should be inserted.
	NoteList::Index NoteList::findInsertionIndex(
		const MessageData semitone) const
	{
		for (Index index{0U}; index < getSize(); ++index) {
			if (semitone <= notes_[index].getSemitone()) {
				return index;
			}
		}

		// Add to end
		return getSize();
	}




	/// Finds the index of the ::MidiIn::Note playing @p semitone within this NoteList.
	/// @param[in] semitone  MIDI semitone index of the ::MidiIn::Note to find.
	/// @returns The index of a matching ::MidiIn::Note, or an index beyond the
	///   end of this NoteList if it wasn't found.
	NoteList::Index NoteList::findIndexBySemitone(
		const MessageData semitone) const
	{
		const Index index{findInsertionIndex(semitone)};
		if (index < getSize()
			&& notes_[index].getSemitone() == semitone)
		{
			return index;  // Found active match
		}

		// Not found
		return getSize();  
	}




	/// Adds @p note to this NoteList, positioned according to its semitone.
	/// @param[in] note  The ::MidiIn::Note to begin playing.
	/// @returns The new position of @p note after addition, or an index beyond
	///   the end of this NoteList if @p note wasn't added.
	/// @see See #removeByIndex()' return behavior for when @p note is silent.
	NoteList::Index NoteList::add(
		const Note &note)
	{
		const Index newIndex{findInsertionIndex(note.getSemitone())};
		if (newIndex < getSize() && notes_[newIndex].getSemitone() == note.getSemitone()) {
			// Already exists

			if (note.isSilent()) {
				remove(newIndex);
				return getSize();  // Removed instead of added
			}

			notes_[newIndex] = note;  // Update all properties
			return getSize();  // Only updated, but not added

		} else if (getSize() >= sizeMax_  // Registry already full
			|| note.isSilent())  // Unrecognized silent note
		{
			return getSize();  // Discard note
		}

		// Shift all higher notes up
		for (Index index{next_++}; index > newIndex; --index) {
			notes_[index] = notes_[index - 1U];
		}

		// Copy new note
		notes_[newIndex] = note;
		return newIndex;
	}




	/// Removes the active ::MidiIn::Note at a given @p index from within this NoteList.
	/// @param[in] index  The position within this NoteList to remove a
	///   ::MidiIn::Note from.
	void NoteList::remove(
		Index index)
	{
		if (index < getSize()) {  // Active note
			--next_;

			// Shift higher notes down to fill its place
			while (index++ < getSize()) {
				notes_[index - 1U] = notes_[index];
			}
		}
	}




	/// Removes the active ::MidiIn::Note playing @p semitone.
	/// @param[in] semitone  The MIDI semitone index to find a match of.
	/// @returns `true` if the given ::MidiIn::Note was found and removed.
	bool NoteList::removeBySemitone(
		const MessageData semitone)
	{
		const Index oldIndex{findIndexBySemitone(semitone)};
		if (oldIndex < getSize()) {  // Found
			remove(oldIndex);
			return true;
		}

		return false;  // Not found
	}
}
