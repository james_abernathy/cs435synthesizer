/// @file
/// ::MidiIn::State class for tracking MIDI input.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_STATE_HPP_
#define MIDIIN_STATE_HPP_


#include "../Observer.hpp"
#include "Types.hpp"
#include "Channel.hpp"
#include "MessageStream.hpp"


namespace MidiIn
{
	/// The state of the entire MIDI module, including all ::MidiIn::Channel%s.
	class State :
		public Observer<MessageStream::MessageCompleted>
	{


		/// Max number of allowed ::MidiIn::Channel%s, set by the MIDI spec.
	public:
		static constexpr ChannelIndex numChannels_{16U};




		/// All channels in this State.
	private:
		Channel channels_[numChannels_];




	public:
		State();

	public:
		void handleMessage(
			const StatusIndex type,
			const ChannelIndex channel,
			const MessageData * const data,
			const std::size_t dataLength);

	public:
		void reset();

	public:
		virtual void notify(
			const MessageStream::MessageCompleted &message);




		/// Allows read access to component ::MidiIn::Channel%s within this State.
		/// @param[in] index  A channel number to retrieve, below #numChannels_.
		/// @returns The requested ::MidiIn::Channel instance, or `nullptr` if
		///   @p index was out of range.
	public:
		const Channel *getChannel(
			const ChannelIndex index) const
		{
			if (index < numChannels_) {
				return &channels_[index];
			}

			return nullptr;  // Doesn't exist
		}
	};
}


#endif  // !defined(MIDIIN_STATE_HPP_)
