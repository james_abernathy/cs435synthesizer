/// @file
/// ::MidiIn::UsbMidiStream class that transcodes 4-byte USB-MIDI messages to a ::MidiIn::MessageStream.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_USBMIDISTREAM_HPP_
#define MIDIIN_USBMIDISTREAM_HPP_


#include <cstdint>

#include "../Uncopyable.hpp"
#include "MessageStream.hpp"
#include "Types.hpp"


namespace MidiIn
{
	/// An interpreter for USB-MIDI message packets which translates them back into a MIDI byte stream.
	class UsbMidiStream :
		private Uncopyable
	{


		/// The cable number of a distinct MIDI stream within a USB-MIDI connection.
		///   USB-MIDI can send more than one stream of messages over a single
		///   interface. These separate cables of data are numbered `0` to `15`.
	public:
		typedef std::uint8_t CableId;




		/// USB-MIDI data is received in *4-byte* "event" blocks, containing a partial or whole MIDI message.
	public:
		static constexpr std::size_t eventLength_{4U};  // bytes




		/// MIDI messages over this USB-MIDI cable will be interpreted and passed to #messageSttream_.
	private:
		const CableId cableId_;

		/// Output stream parser to pass MIDI message bytes to.
	private:
		MessageStream &messageStream_;




		/// Initializes this stream to parse USB-MIDI messages on @p cableId and pass them to @p messageStream.
	public:
		UsbMidiStream(
			MessageStream &messageStream,
			const CableId cableId = 0U);




		/// Parses a 4-byte USB-MIDI event containing part of a MIDI message.
		/// @param[in] messageDataBuffer  Buffer with at least four bytes to read.
	public:
		void parseMessageData(
			const std::uint8_t *messageDataBuffer);
	};
}


#endif  // !defined(MIDIIN_USBMIDISTREAM_HPP_)
