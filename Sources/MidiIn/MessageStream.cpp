/// @file
/// ::MidiIn::MessageStream class that interprets MIDI serial input as MIDI messages.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "MessageStream.hpp"

#include <limits>


namespace MidiIn
{

		const StatusIndex MessageStream::runningStatusNone_{
			std::numeric_limits<StatusIndex>::max()};




	/// Initializes the MessageStream with no received bytes.
	MessageStream::MessageStream() :
		data_{},
		messageCompleted_{*this}
	{
		clearRunningStatus();
	}




	/// Interprets this MessageStream's next byte.
	/// @param[in] byte  The next input byte to parse.
	void MessageStream::parseByte(
		const std::uint8_t byte)
	{
		if (byte > maxMessageDataValue) {  // Status identifier
			return parseByteStatus(
				static_cast<StatusIndex>(byte >> 4U),
				static_cast<ChannelIndex>(byte & 0xF));

		} else if (runningStatusType_) {  // Non-ignored message data
			return parseByteData(
				static_cast<MessageData>(byte));
		}
	}




	/// Interprets this MessageStream's next message status identifier byte.
	/// @param[in] newStatusType  The new status byte's high nibble.
	/// @param[in] newStatusChannel  The new status byte's low nibble.
	void MessageStream::parseByteStatus(
		const StatusIndex newStatusType,
		const ChannelIndex newStatusChannel)
	{
		if (newStatusType == MessageTypes::System) {
			if (newStatusChannel <= SystemMessageTypes::maxSystemCommon) {

				// System common
				if (runningStatusType_ == MessageTypes::System
					&& runningStatusChannel_ == SystemMessageTypes::SystemExclusive)
				{
					// Unfinished SysEx message
					// Fire in-progress SysEx message
					setCompletedMessage(
						runningStatusType_, runningStatusChannel_, runningDataLength_);
					if (newStatusChannel == SystemMessageTypes::TuneRequest) {
						// 1-byte message
						setCompletedMessage(runningStatusType_, runningStatusChannel_);

					} else if (newStatusChannel != SystemMessageTypes::SystemExclusiveEnd) {
						// Begin next message
						setRunningStatus(newStatusType, newStatusChannel);
					}

				} else if (newStatusChannel == SystemMessageTypes::SystemExclusiveEnd) {
					// No SysEx message to end
					// Completely ignore it

				} else if (newStatusChannel == SystemMessageTypes::TuneRequest) {
					// 1-byte system common message
					setCompletedMessage(newStatusType, newStatusChannel);

				} else {
					// Multi-byte system common message
					setRunningStatus(newStatusType, newStatusChannel);
				}

			} else {
				// System realtime
				// Realtime messages are only one byte long, and can be interspersed in other message types
				if (newStatusChannel == SystemMessageTypes::Reset) {
					clearRunningStatus();  // Reset stream's state
				}
				setCompletedMessage(newStatusType, newStatusChannel);
			}

		} else {
			// Voice category
			if (runningStatusType_ == MessageTypes::System
				&& runningStatusChannel_ == SystemMessageTypes::SystemExclusive)
			{
				// SysEx message in progress
				// Fire finished SysEx message and begin next message
				setCompletedMessage(
					runningStatusType_, runningStatusChannel_, runningDataLength_);
				setRunningStatus(newStatusType, newStatusChannel);

			} else {
				// Multi-byte voice message
				setRunningStatus(newStatusType, newStatusChannel);
			}
		}
	}




	/// Interprets this MessageStream's next data byte in the context of the current running status.
	/// @param[in] byte  The new data byte to parse.
	void MessageStream::parseByteData(
		const MessageData byte)
	{
		if (runningStatusType_ == runningStatusNone_) {
			// Not expecting data
			return;  // Ignore data

		} else if (runningDataLength_ >= maxDataBytes_) {
			// Buffer was already full
			clearRunningStatus();  // Completely ignore overflowed message and further data bytes
			return;
		}

		// Save byte and check if current message is full
		data_[runningDataLength_++] = byte;
		if (runningDataLength_ == 1U) {
			// Complete 2-byte messages
			switch (runningStatusType_) {
				case MessageTypes::System: {
					if (runningStatusChannel_ != SystemMessageTypes::MTCQuarterFrame
						&& runningStatusChannel_ != SystemMessageTypes::SongSelect)
					{
						break;  // Not a 2-byte system common message
					}
				}
				case MessageTypes::Program:
				case MessageTypes::ChannelPressure: {
					setCompletedMessage(
						runningStatusType_, runningStatusChannel_, runningDataLength_);
				}
			}

		} else if (runningDataLength_ == 2U) {
			// Complete 3-byte messages
			switch (runningStatusType_) {
				case MessageTypes::System: {
					if (runningStatusChannel_ != SystemMessageTypes::SongPositionPointer) {
						break;  // Not a 3-byte system common message
					}
				}
				case MessageTypes::NoteOff:
				case MessageTypes::NoteOn:
				case MessageTypes::AfterTouch:
				case MessageTypes::Controller:
				case MessageTypes::PitchWheel: {
					setCompletedMessage(
						runningStatusType_, runningStatusChannel_, runningDataLength_);
				}
			}

		} else if (runningStatusType_ != MessageTypes::System
			|| runningStatusChannel_ != SystemMessageTypes::SystemExclusive)
		{
			// SysEx messages are the only ones that can contain more than 2 data bytes!
			clearRunningStatus();  // Clear and ignore
		}
	}




	/// Short-hand for notifying ::Project::Observer%s of a finished message and clearing running status.
	void MessageStream::setCompletedMessage(
		const StatusIndex statusType,
		const ChannelIndex statusChannel,
		const DataIndex dataLength)
	{
		messageCompleted_.notifyObservers(statusType, statusChannel, dataLength);

		if (statusType == MessageTypes::System
			&& statusChannel <= SystemMessageTypes::maxSystemCommon)
		{
			// System common message, which clears running status
			clearRunningStatus();

		} else {
			runningDataLength_ = 0U;  // Continue filling data for the running status
		}
	}




	/// Sets a new running message status to interpret following data bytes for.
	void MessageStream::setRunningStatus(
		const StatusIndex statusType,
		const ChannelIndex statusChannel)
	{
		runningStatusType_ = statusType;
		runningStatusChannel_ = statusChannel;
		runningDataLength_ = 0U;
	}

	/// Clears the running status, ignoring further data bytes.
	void MessageStream::clearRunningStatus()
	{
		setRunningStatus(runningStatusNone_, 0U);
	}

	/// Notifies listening ::Project::Observer%s when a new MIDI message is ready.
	/// @param[in] statusType  This message's high status byte nibble.
	/// @param[in] statusChannel  This message's low status byte nibble.
	/// @param[in] dataLength  The length of this message's data within #messageStream_.
	void MessageStream::MessageCompleted::notifyObservers(
		const StatusIndex statusType,
		const ChannelIndex statusChannel,
		const DataIndex dataLength)
	{
		statusType_ = statusType;
		statusChannel_ = statusChannel;
		dataLength_ = dataLength;
		notifyObservers();
	}
}
