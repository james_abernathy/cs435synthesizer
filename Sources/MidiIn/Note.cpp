/// @file
/// ::MidiIn::Note class, representing a single MIDI note.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "Note.hpp"

#include <limits>


namespace MidiIn
{
		const MessageData Note::pressureInherited_{
			std::numeric_limits<MessageData>::max()};
}
