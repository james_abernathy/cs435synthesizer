/// @file
/// Common type definitions used throughout the ::MidiIn namespace.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_TYPES_HPP_
#define MIDIIN_TYPES_HPP_


#include <cstddef>
#include <cstdint>


namespace MidiIn
{
	/// Data type of MIDI message values.
	///   If the value is greater than ::MidiIn::maxMessageDataValue, then it
	///   represents a status byte rather than data.
	typedef std::uint8_t MessageData;

	/// 14-bit <dfn>continuous</dfn> MIDI data value, made up of coarse and fine 7-bit halves.
	typedef std::uint16_t Continuous;


	/// Data type of 4-bit MIDI status indexes (the upper nibble of ::MidiIn::MessageData).
	typedef std::uint8_t StatusIndex;

	/// Data type of 4-bit ::MidiIn::Channel indexes within a ::MidiIn::State
	///   This is transmitted as the lower nibble of status ::MidiIn::MessageData.
	typedef std::uint8_t ChannelIndex;


	/// Represents times in milliseconds.
	typedef std::uint32_t Time_ms;

	/// Represents times in hardware timer clock ticks.
	typedef std::uint32_t Time_clk;


	/// Represents relative pitches in <dfn>cents</dfn>: hundredths of a semitone.
	typedef std::int16_t Cents;

	/// Represents a linear volume between `0.0` and `1.0`.
	typedef float Volume;




	/// How many bits are used to represent message data values.
	constexpr std::uint8_t numMessageDataBits{7U};

	/// The max value for message data bytes.
	///   Higher values are reserved for status identifiers.
	constexpr MessageData maxMessageDataValue{
		(1U << numMessageDataBits) - 1U};

	/// The max possible value for continuous controllers.
	///   Continuous values are composed of course and fine message bytes.
	constexpr Continuous maxContinuousValue{static_cast<Continuous>(
		(1U << (2U * numMessageDataBits)) - 1U)};




	/// Contains enumerated constants for all MIDI message types.
	namespace MessageTypes
	{
		/// Message types corresponding to the high nibbles of status bytes.
		enum Enum :
			StatusIndex
		{
			NoteOff         = 0x8,
			NoteOn          = 0x9,
			AfterTouch      = 0xA,
			Controller      = 0xB,
			Program         = 0xC,
			ChannelPressure = 0xD,
			PitchWheel      = 0xE,
			System          = 0xF,
		};
	}


	/// Contains enumerated constants for all MIDI system message sub-types.
	namespace SystemMessageTypes
	{
		/// Low nibble values for system messages, which take the place of the usual channel number of voice messages.
		enum Enum :
			ChannelIndex
		{
			// System common
			SystemExclusive     = 0x00,
			MTCQuarterFrame     = 0x01,
			SongPositionPointer = 0x02,
			SongSelect          = 0x03,
			TuneRequest         = 0x06,
			SystemExclusiveEnd  = 0x07,

			// System realtime
			Clock               = 0x08,
			Start               = 0x0A,
			Continue            = 0x0B,
			Stop                = 0x0C,
			ActiveSense         = 0x0E,
			Reset               = 0x0F,
		};

		/// System message channel values higher than this are realtime instead of common.
		constexpr ChannelIndex maxSystemCommon{SystemExclusiveEnd};
	}


	/// Contains enumerated constants for all MIDI controller message sub-types.
	namespace ControllerTypes
	{
		/// Defined controller message function IDs.
		enum Enum :
			MessageData
		{
			// Continuous coarse control
			BankSelectCoarse             =   0U,
			ModulationWheelCoarse        =   1U,
			BreathCoarse                 =   2U,
			FootPedalCoarse              =   4U,
			PortamentoTimeCoarse         =   5U,
			DataEntryCoarse              =   6U,
			VolumeCoarse                 =   7U,
			BalanceCoarse                =   8U,
			PanPositionCoarse            =  10U,
			ExpressionCoarse             =  11U,
			Effect1Coarse                =  12U,
			Effect2Coarse                =  13U,
			GeneralPurposeSlider1        =  16U,
			GeneralPurposeSlider2        =  17U,
			GeneralPurposeSlider3        =  18U,
			GeneralPurposeSlider4        =  19U,

			// Continuous fine control
			BankSelectFine               =  32U,
			ModulationWheelFine          =  33U,
			BreathFine                   =  34U,
			FootPedalFine                =  36U,
			PortamentoTimeFine           =  37U,
			DataEntryFine                =  38U,
			VolumeFine                   =  39U,
			BalanceFine                  =  40U,
			PanPositionFine              =  42U,
			ExpressionFine               =  43U,
			Effect1Fine                  =  44U,
			Effect2Fine                  =  45U,

			// Switches (on/off)
			HoldPedal                    =  64U,
			Portamento                   =  65U,
			SustenutoPedal               =  66U,
			SoftPedal                    =  67U,
			LegatoPedal                  =  68U,
			Hold2Pedal                   =  69U,

			// General purpose
			SoundVariation               =  70U,
			SoundTimbre                  =  71U,
			SoundReleaseTime             =  72U,
			SoundAttackTime              =  73U,
			SoundBrightness              =  74U,
			SoundControl6                =  75U,
			SoundControl7                =  76U,
			SoundControl8                =  77U,
			SoundControl9                =  78U,
			SoundControl10               =  79U,
			Button1                      =  80U,
			Button2                      =  81U,
			Button3                      =  82U,
			Button4                      =  83U,
			EffectsLevel                 =  91U,
			TremuloLevel                 =  92U,
			ChorusLevel                  =  93U,
			CelesteLevel                 =  94U,
			PhaserLevel                  =  95U,
			DataButtonIncrement          =  96U,
			DataButtonDecrement          =  97U,
			NonRegisteredParameterFine   =  98U,
			NonRegisteredParameterCoarse =  99U,
			RegisteredParameterFine      = 100U,
			RegisteredParameterCoarse    = 101U,

			// Special commands
			AllSoundsOff                 = 120U,
			AllControllersOff            = 121U,
			LocalKeyboard                = 122U,
			AllNotesOff                  = 123U,
			OmniModeOff                  = 124U,
			OmniModeOn                   = 125U,
			MonoOperation                = 126U,
			PolyOperation                = 127U,
		};
	}


	/// Contains enumerated constants for all officially registered parameter numbers.
	namespace RegisteredParameterTypes
	{
		enum Enum :
			Continuous
		{
			PitchBendSensitivity = 0U,
			TuningFine           = 1U,
			TuningCoarse         = 2U,
			TuningProgramSelect  = 3U,
			TuningBankSelect     = 4U,

			/// Special parameter value indicating that [N]RPN-related controllers will be ignored.
			None = maxContinuousValue,
		};
	}




	/// Joins two MIDI data values into a ::MidiIn::Continuous value.
	/// @param[in] dataHigh  The most-significant data value, between `0` and
	///   ::maxMessageDataValue.
	/// @param[in] dataLow  The least-significant data value, between `0` and
	///   ::maxMessageDataValue.
	/// @returns The combination of @p dataHigh with @p dataLow.
	constexpr Continuous joinContinuous(
		const MessageData dataHigh,
		const MessageData dataLow)
	{
		return static_cast<Continuous>((dataHigh << numMessageDataBits) | dataLow);
	}




	/// Gets the least-significant half of a ::MidiIn::Continuous value.
	/// @param[in] continuous  Continuous data value to read from.
	/// @returns The least-significant half of @p continuous.
	constexpr MessageData getContinuousFine(
		const Continuous continuous)
	{
		return static_cast<MessageData>(continuous & maxMessageDataValue);
	}


	/// Gets the most-significant half of a ::MidiIn::Continuous value.
	/// @param[in] continuous  Continuous data value to read from.
	/// @returns The most-significant half of @p continuous.
	constexpr MessageData getContinuousCoarse(
		const Continuous continuous)
	{
		return static_cast<MessageData>(continuous >> numMessageDataBits);
	}




	/// @vrief Replaces the least-significant half of a ::MidiIn::Continuous value.
	/// @param[in] continuous  Continuous data value to be modified.
	/// @param[in] dataLow  New least-significant data value to combine with
	///   @p continuous.
	/// @returns A new continuous data value with @p continuous' most-significant
	///   data value joined with @p dataLow.
	constexpr Continuous setContinuousFine(
		const Continuous continuous,
		const MessageData dataLow)
	{
		return joinContinuous(getContinuousCoarse(continuous), dataLow);
	}


	/// Replaces the most-significant half of a ::MidiIn::Continuous value.
	/// @param[in] continuous  Continuous data value to be modified.
	/// @param[in] dataHigh  New most-significant data value to combine with
	///   @p continuous.
	/// @returns A new continuous data value with @p dataHigh joined with
	///   @p continuous' least-significant data value.
	constexpr Continuous setContinuousCoarse(
		const Continuous continuous,
		const MessageData dataHigh)
	{
		return joinContinuous(dataHigh, getContinuousFine(continuous));
	}
}


#endif  // !defined(MIDIIN_TYPES_HPP_)
