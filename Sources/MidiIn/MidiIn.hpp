/// @file
/// A library of classes and resources for building a MIDI state from streamed messages.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_MIDIIN_HPP_
#define MIDIIN_MIDIIN_HPP_


/// Classes and resources for building a MIDI state from streamed messages.
namespace MidiIn
{
}


#include "Types.hpp"
#include "Constants.hpp"

#include "Note.hpp"
#include "NoteList.hpp"
#include "NotePeriods.hpp"


#include "Channel.hpp"
#include "State.hpp"


#include "MessageStream.hpp"
#include "UsbMidiDevice.hpp"


#endif   // !defined(MIDIIN_MIDIIN_HPP_)
