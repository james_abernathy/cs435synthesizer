/// @file
/// ::MidiIn::Channel class, which keeps track of active notes and channel-wide properties.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_CHANNEL_HPP_
#define MIDIIN_CHANNEL_HPP_


#include "../Observable.hpp"
#include "../Uncopyable.hpp"
#include "Types.hpp"
#include "Constants.hpp"
#include "Note.hpp"
#include "NoteList.hpp"


namespace MidiIn
{
	/// One of the 16 available MIDI channels, which tracks active notes and channel-wide properties.
	class Channel :
		private Uncopyable
	{


		/// Represents the position of notes within a ::MidiIn::Channel.
	public:
		typedef NoteList::Index NoteIndex;


		/// Channel note pressure used when controllers are reset.
	private:
		static constexpr MessageData pressureDefault_{0U};

		/// Channel volume used when controllers are reset.
	private:
		static constexpr MessageData volumeDefault_{100U};

		/// Channel expression used when controllers are reset.
	private:
		static constexpr MessageData expressionDefault_{maxMessageDataValue};

		/// Middle pitch bend value where no bending occurs; #pitchBend_ is relative to this.
	private:
		static constexpr Continuous pitchBendNeutral_{0x2000};

		/// Default pitch bend range in cents above and below neutral.
	private:
		static constexpr Cents pitchBendRangeDefault_{2 * centsPerSemitone};

		/// Release velocity used when removing ::MidiIn::Note%s in cases where it isn't specified.
	private:
		static constexpr MessageData releaseDefault_{maxMessageDataValue};




		/// Active ::MidiIn:Note%s playing on this Channel.
	private:
		NoteList notes_;

		/// This Channel's main volume property, between `0` (silent) and ::MidiIn::maxMessageDataValue.
		///   Volume, as opposed to #expression_, is the primary loudness control
		///   for changes relative to other Channels. For example, it can bring one
		///   part into focus without changing that part's original expression values.
		/// @see The secondary, intra-Channel loudness is #expression_.
	private:
		MessageData volume_;

		/// This Channel's secondary volume property, between `0` (silent) and ::MidiIn::maxMessageDataValue.
		///   Expression is the secondary loudness control after #volume_, used for
		///   changes within a part such as crescendos and decrescendos.
		/// @see The primary inter-Channel loudness is #volume_.
	private:
		MessageData expression_;

		/// The cached, combined loudness of #volume_ and #expression_.
		/// @see #getVolume()
	private:
		Volume volumeEffective_;

		/// Raw 14-bit pitch bend amount, centered around #pitchBendNeutral_.
	private:
		Continuous pitchBend_;

		/// Maximum pitch bend magnitude in cents to scale #pitchBend_ by.
	private:
		Cents pitchBendRange_;

		/// Calculated pitch bend for all ::MidiIn::Note%s on this Channel, in cents.
	private:
		Cents pitchBendCents_;

		/// Which parameter number will be affected by ::MidiIn::ControllerTypes::Enum::DataEntryCoarse and other [N]RPN-related controllers.
		///   When set to ::MidiIn::RegisteredParameterTypes::Enum::None, this
		///   Channel ignores registered and non-registered parameter number
		///   controllers. When the #parameterRegistered_ boolean is `true`, this
		///   number affects non-registered parameters instead of registered ones.
	private:
		Continuous parameter_;

		/// Flag indicating when #parameter_ represents a registered parameter number.
		///   A value of `true` indicates a registered parameter number (RPN), and
		///   `false` represents a non-registered one (NRPN).
	private:
		bool parameterRegistered_;


		/// @def MIDIIN_CHANNEL_NO_PROGRAM
		/// Reduces the size of ::MidiIn::Channel%s by omitting program and program bank bookkeeping.
		///   If memory is scarce, this can save 80 bytes per ::MidiIn::State.
#ifndef MIDIIN_CHANNEL_NO_PROGRAM

		/// Pending #bank_ value for future program change messages.
		/// @note This member is only available if ::MIDIIN_CHANNEL_NO_PROGRAM is undefined.
	private:
		Continuous bankPending_;

		/// Bank of programs that this Channel is currently playing from.
		/// @note This member is only available if ::MIDIIN_CHANNEL_NO_PROGRAM is undefined.
	private:
		Continuous bank_;

		/// Program within #bank_ that this Channel is currently playing as.
		/// @note This member is only available if ::MIDIIN_CHANNEL_NO_PROGRAM is undefined.
	private:
		MessageData program_;
#endif  // !defined(MIDIIN_CHANNEL_NO_PROGRAM)


#ifndef MIDIIN_NOTE_NO_PRESSURE
		/// Channel pressure that applies to all ::MidiIn::Note%s with no after-touch.
		/// @note This member is only available if ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	private:
		MessageData pressure_;
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)




		/// Observer-pattern subject that notifies ::Project::Observer%s when a ::MidiIn::Note was added to the containing Channel.
	public:
		class NoteAdded :
			public Observable<NoteAdded>
		{

			friend class Channel;


			/// The new ::MidiIn::Note's index within this ::Project::Observable's ::MidiIn::Channel.
		private:
			NoteIndex index_;


		private:
			using Observable<NoteAdded>::notifyObservers;

		private:
			NoteAdded();

		private:
			void notifyObservers(
				const NoteIndex index);

		public:
			NoteIndex getIndex() const;

		} noteAdded_;  ///< Notifies ::Project::Observer%s when a ::MidiIn::Note was added to this Channel.




#ifndef MIDIIN_NOTE_NO_PRESSURE
		/// Observer-pattern subject that notifies ::Project::Observer%s when one of the containing Channel's ::MidiIn::Note%s changes pressure.
		/// @note This class is only available if ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	public:
		class NotePressureChanged :
			public Observable<NotePressureChanged>
		{

			friend class Channel;


			/// The changed ::MidiIn::Note's index within this ::Project::Observable's ::MidiIn::Channel.
		private:
			NoteIndex index_;


		private:
			using Observable<NotePressureChanged>::notifyObservers;

		private:
			NotePressureChanged();

		private:
			void notifyObservers(
				const NoteIndex index);

		public:
			NoteIndex getIndex() const;

		} notePressureChanged_;  ///< Notifies ::Project::Observer%s when one of this Channel's ::MidiIn::Note%s changes pressure.
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)




		/// Observer-pattern subject that notifies ::Project::Observer%s when the containing Channel removes a ::MidiIn::Note.
	public:
		class NoteRemoved :
			public Observable<NoteRemoved>
		{
			friend class Channel;


			/// A copy of the removed ::MidiIn::Note for ::Project::Observer%s to reference.
			///   Set to `nullptr` when not notifying.
		private:
			const Note *note_;

			/// The removed ::MidiIn::Note's release velocity to stop with.
		private:
			MessageData release_;


		private:
			using Observable<NoteRemoved>::notifyObservers;

		private:
			NoteRemoved();

		private:
			void notifyObservers(
				const Note &note,
				const MessageData release);

		public:
			const Note *getNote() const;

		public:
			MessageData getRelease() const;

		} noteRemoved_;  ///< Notifies ::Project::Observer%s when this Channel removes a ::MidiIn::Note.




		/// Observer-pattern subject that notifies ::Project::Observer%s when the containing Channel removes all ::MidiIn::Note at once.
	public:
		class NotesCleared :
			public Observable<NotesCleared>
		{

			friend class Channel;


		private:
			NotesCleared();

		} notesCleared_;  ///< Notifies ::Project::Observer%s when this Channel removes all ::MidiIn::Note%s at once.




		/// Observer-pattern subject that notifies ::Project::Observer%s when one of the containing Channel's sound properties changes, such as volume.
	public:
		class SoundChanged :
			public Observable<SoundChanged>
		{

			friend class Channel;


		private:
			SoundChanged();

		} propertyChanged_;  ///< Notifies ::Project::Observer%s when one of this Channel's sound properties changes, such as volume.




	private:
		static Continuous getPitchBendRangeParameter(
			const Cents centsTotal);

	private:
		static Cents getPitchBendRangeCents(
			const Continuous parameter);




	public:
		Channel();


	public:
		void reset();

	public:
		void resetNotes();

	public:
		void resetControllers();


	private:
		void setPitchBend(
			const Continuous pitchBend,
			const Cents pitchBendRange);

	private:
		void setVolumeEffective(
			const MessageData volume,
			const MessageData expression);


	private:
		bool getParameterValue(
			Continuous &value) const;

	private:
		void setParameterValue(
			const Continuous value);


#ifndef MIDIIN_NOTE_NO_PRESSURE
	private:
		void messageAfterTouchByIndex(
			const NoteIndex index,
			const MessageData notePressure);
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)


	public:
		void messageNoteOn(
			const MessageData semitone,
			const MessageData attack);

	public:
		void messageNoteOff(
			const MessageData semitone,
			const MessageData release);

	public:
		void messagePitchWheel(
			const MessageData bendLow,
			const MessageData bendHigh);


#ifndef MIDIIN_CHANNEL_NO_PROGRAM
	public:
		void messageProgramChange(
			const MessageData program);
#endif  // !defined(MIDIIN_CHANNEL_NO_PROGRAM)


#ifndef MIDIIN_NOTE_NO_PRESSURE
	public:
		void messageAfterTouch(
			const MessageData semitone,
			const MessageData notePressure);

	public:
		void messageChannelPressure(
			const MessageData pressure);
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)


	public:
		void messageController(
			const MessageData controller,
			const MessageData data);

	public:
		void handleMessage(
			const StatusIndex type,
			const MessageData * const data,
			const std::size_t dataLength);




		/// @returns This Channel's ::MidiIn::NoteList for reading active ::MidiIn::Note%s.
		/// @see #notes_
	public:
		const NoteList &getNotes() const
		{
			return notes_;
		}




		/// @returns This Channel's pitch bend, in cents.
		/// @see #pitchBendCents_
	public:
		Cents getPitchBendCents() const
		{
			return pitchBendCents_;
		}




		/// @returns The effective loudness of this Channel between `0.0` and `1.0`.
		/// @see #volumeEffective_
	public:
		Volume getVolume() const
		{
			return volumeEffective_;  // Cached value, since synth may poll this often
		}




#ifndef MIDIIN_NOTE_NO_PRESSURE
		/// Gets the combined after-touch and Channel pressure to play the given ::MidiIn::Note at.
		/// @param[in] index  The desired ::MidiIn::Note's index within this Channel.
		/// @returns A pressure value between `0.0` and `1.0`.
		/// @note This method is only available if ::MIDIIN_NOTE_NO_PRESSURE is undefined.
	public:
		Volume getEffectiveNotePressure(
			const NoteIndex index) const
		{
			const Note * const note{notes_.getNote(index)};
			if (!note) {  // Doesn't exist
				return 0U;  // Placeholder's better than a null dereference
			}

			const MessageData notePressure{note->getPressure()};
			if (notePressure == Note::pressureInherited_) {
				return static_cast<Volume>(pressure_) / maxMessageDataValue;
			}

			return static_cast<Volume>(notePressure) / maxMessageDataValue;
		}
#endif  // !defined(MIDIIN_NOTE_NO_PRESSURE)




#ifndef MIDIIN_CHANNEL_NO_PROGRAM
		/// @returns This Channel's active program, or instrument, within its bank.
		/// @see #program_
		/// @see #getProgramBank()
		/// @note This method is only available if ::MIDIIN_CHANNEL_NO_PROGRAM is undefined.
	public:
		MessageData getProgram() const
		{
			return program_;
		}




		/// @returns This Channel's active bank containing its program.
		/// @see #bank_
		/// @see #getProgram()
		/// @note This method is only available if ::MIDIIN_CHANNEL_NO_PROGRAM is undefined.
	public:
		Continuous getProgramBank() const
		{
			return bank_;
		}
#endif  // !defined(MIDIIN_CHANNEL_NO_PROGRAM)
	};
}


#endif  // !defined(MIDIIN_CHANNEL_HPP_)
