/// @file
/// ::MidiIn::NotePeriods static class containing pre-compiled note periods.
///
/// @copyright Copyright © 2013-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef MIDIIN_NOTEPERIODS_HPP_
#define MIDIIN_NOTEPERIODS_HPP_


#include "Constants.hpp"
#include "Types.hpp"


namespace MidiIn
{
	/// Static class for quickly accessing pre-calculated note periods.
	///   Because AVR-GCC limits the maximum byte size of any address to
	///   ::MAX_INT, the full list of pre-calculated ::std::uint32_t%s must be
	///   split into multiple slices.
	class NotePeriods
	{


		/// Index of period array slices within #slices_.
	private:
		typedef std::uint8_t SliceIndex;

		/// Index of periods within sub-arrays of #slices_.
	private:
		typedef std::uint16_t PeriodIndex;




		/// Sentinel invalid period returned by #getPeriod() for a pitch that wasn't pre-calculated.
	private:
		static constexpr Time_clk uncalculatedPeriod_{0U};




		/// Reference cent value of the first frequency within the first sub-array of #slices_ which all others are relative to.
	private:
		static const Cents startCent_;

		/// How many least significant bits of a cent index make up an index into one of #slices_' sub-arrays of periods.
	private:
		static const PeriodIndex periodIndexBits_;

		/// How many sub-arrays were necessary to store all pre-calculated periods.
	private:
		static const SliceIndex sliceCount_;

		/// Length of all sub-arrays within #slices_ except for the last one, which is #lastSliceLength_ long.
	private:
		static const PeriodIndex periodsPerSlice_;

		/// Length of the last slice within #slices_.
	private:
		static const PeriodIndex lastSliceLength_;

		/// Array of #sliceCount_ sub-arrays of periods.
	private:
		static const Time_clk * const slices_[];




		/// Gets the period of a given MIDI index @p semitone in nanoseconds, bent by @p cents.
		/// @param[in] semitone  MIDI semitone index to find the period of.
		/// @param[in] cents  Number of cents (hundredths of a semitone) to adjust
		///   @p semitone by before finding its period.
		/// @returns The period of the given @p semitone after applying @p cents, or
		///   `0` if it isn't pre-calculated.
	public:
		static Time_clk getPeriod(
			const std::int16_t semitone,
			const Cents cents)
		{
			const Cents centIndex{static_cast<Cents>(
				semitone * centsPerSemitone + cents - startCent_)};
			if (centIndex < 0) {
				// Below pre-calculated range
				return uncalculatedPeriod_;
			}

			static const PeriodIndex periodIndexMask{static_cast<PeriodIndex>(
				(1U << periodIndexBits_) - 1U)};

			const SliceIndex sliceIndex{static_cast<SliceIndex>(
				centIndex >> periodIndexBits_)};
			const PeriodIndex periodIndex{static_cast<PeriodIndex>(
				centIndex & periodIndexMask)};

			if (sliceIndex > sliceCount_
				|| (sliceIndex == sliceCount_ && periodIndex > lastSliceLength_))
			{
				// Above pre-calculated range
				return uncalculatedPeriod_;
			}

			return slices_[sliceIndex][periodIndex];
		}
	};
}


#endif // !defined(MIDIIN_NOTEPERIODS_HPP_)
