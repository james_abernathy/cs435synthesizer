/// @file
/// ::Project::UsbMidiDevice class for acting as a USB MIDI destination port.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "UsbMidiDevice.hpp"

#include <cstddef>

#include "USBDevice/USBDevice/USBDescriptor.h"
#include "USBDevice/USBAudio/USBAudio_Types.h"


namespace Project
{
	UsbMidiDevice::UsbMidiDevice(
		MidiIn::State &midiState,
		MidiIn::UsbMidiStream &usbMidiStream,
		const std::uint16_t vendorId,
		const std::uint16_t productId,
		const std::uint16_t productRelease
	) :
		USBDevice{vendorId, productId, productRelease},
		midiState_{midiState},
		usbMidiStream_{usbMidiStream}
	{
	}




	bool UsbMidiDevice::EPBULK_OUT_callback()
	{
		// Buffer received bulk-transferred events
		std::uint8_t eventBuffer[MAX_PACKET_SIZE_EPBULK];
		std::uint32_t eventBufferSize;

		if (readEP(EPBULK_OUT, eventBuffer, &eventBufferSize, MAX_PACKET_SIZE_EPBULK)) {

			// Process each event's contents
			for (decltype(eventBufferSize) eventIndex{0U};
				eventIndex + (usbMidiStream_.eventLength_ - 1) < eventBufferSize;  // At least a whole event
				eventIndex += usbMidiStream_.eventLength_)
			{
				usbMidiStream_.parseMessageData(&eventBuffer[eventIndex]);
			}
		}


		// Reactivate endpoint to receive next characters
		if (!readStart(EPBULK_OUT, MAX_PACKET_SIZE_EPBULK)) {
			// ??
		}
		return true;
	}




	bool UsbMidiDevice::USBCallback_setConfiguration(
		std::uint8_t configuration)
	{
		if (configuration != DEFAULT_CONFIGURATION) {
			return false;  // Unsupported
		}

		// Configure at least one endpoint
		addEndpoint(EPBULK_OUT, MAX_PACKET_SIZE_EPBULK);

		// Activate the endpoint to be able to receive data
		readStart(EPBULK_OUT, MAX_PACKET_SIZE_EPBULK);
		return true;
	}




	std::uint8_t *UsbMidiDevice::stringIinterfaceDesc()
	{
		static std::uint8_t interfaceDescriptor[] = {
			12U,  // Length
			STRING_DESCRIPTOR,
			'A', 0U, 'u', 0U, 'd', 0U, 'i', 0U, 'o', 0U
		};
		return interfaceDescriptor;
	}




	std::uint8_t *UsbMidiDevice::stringImanufacturerDesc()
	{
		static std::uint8_t manufacturerDescriptor[] = {
			45U,  // Length
			STRING_DESCRIPTOR,
			'C', 0U, 'S', 0U, 'U', 0U, 'S', 0U, 'M', 0U, ' ', 0U,
			'C', 0U, 'S', 0U, '4', 0U, '3', 0U, '5', 0U, ' ', 0U,
			'F', 0U, 'a', 0U, 'l', 0U, 'l', 0U, ' ', 0U,
			'2', 0U, '0', 0U, '1', 0U, '8', 0U, 
		};
		return manufacturerDescriptor;
	}




	std::uint8_t *UsbMidiDevice::stringIproductDesc()
	{
		static std::uint8_t productDescriptor[] = {
			24U,  // Length
			STRING_DESCRIPTOR,
			'S', 0U, 'y', 0U, 'n', 0U, 't', 0U, 'h', 0U, 'e', 0U, 's', 0U, 'i', 0U, 'z', 0U, 'e', 0U, 'r', 0U
		};
		return productDescriptor;
	}




	std::uint8_t *UsbMidiDevice::configurationDesc()
	{
		static constexpr std::uint8_t inputControlIfaceLength{6U};
		static constexpr std::uint8_t streamingEndpointLength{9U};
		static constexpr std::uint8_t jackEndpointLength{5U};

		static constexpr std::uint16_t totalControlIfaceLength{
			CONTROL_INTERFACE_DESCRIPTOR_LENGTH};
		static constexpr std::uint16_t totalStreamingIfaceLength{
			STREAMING_INTERFACE_DESCRIPTOR_LENGTH +
			inputControlIfaceLength +
			streamingEndpointLength +
			jackEndpointLength};
		static constexpr std::uint16_t totalConfigLength{
			CONFIGURATION_DESCRIPTOR_LENGTH +
			INTERFACE_DESCRIPTOR_LENGTH +
			totalControlIfaceLength +
			INTERFACE_DESCRIPTOR_LENGTH +
			totalStreamingIfaceLength};


		static constexpr std::uint16_t audioControlSpec{0x0100};  // 1.0.0 in BCD

		static constexpr std::uint8_t midiInJackId{1U};


		static std::uint8_t configurationDescriptor[] = {
			// Configuration 1 descriptor
			CONFIGURATION_DESCRIPTOR_LENGTH,
			CONFIGURATION_DESCRIPTOR,
			LSB(totalConfigLength), MSB(totalConfigLength),
			2U,  // Total interfaces
			DEFAULT_CONFIGURATION,  // Configuration number
			0U,  // Configuration string index: None
			C_RESERVED | C_SELF_POWERED,  // Configuration attributes
			C_POWER(100),  // Max power consumption in mA


			// Interface 0, Alternate 0: Standard Audio Control Descriptor
			INTERFACE_DESCRIPTOR_LENGTH,
			INTERFACE_DESCRIPTOR,
			0U, 0U,  // Interface 0, Alternate 0
			0U,  // Total endpoints
			AUDIO_CLASS, SUBCLASS_AUDIOCONTROL,
			0x00,  // Audio Control protocol
			0U,  // Interface string index: None

			// Class-specific Audio Control Interface Header Descriptor
			CONTROL_INTERFACE_DESCRIPTOR_LENGTH,
			INTERFACE_DESCRIPTOR_TYPE,
			CONTROL_HEADER,  // Audio class specific control interface header
			LSB(audioControlSpec), MSB(audioControlSpec),
			LSB(totalControlIfaceLength), MSB(totalControlIfaceLength),
			0x01,  // In collection
			0x01,  // Interface number: Audio stream


			// Interface 1, Alternate 0: MIDI Streaming Descriptor
			INTERFACE_DESCRIPTOR_LENGTH,
			INTERFACE_DESCRIPTOR,
			1U, 0U,  // Interface 1, Alternate 0
			1U,  // Total endpoints
			AUDIO_CLASS, 0x03,  // Audio Streaming subclass
			0x00,  // Streaming Audio protocol
			0U,  // Interface string index: None

			// Class-Specific MIDI Streaming Interface Header Descriptor
			STREAMING_INTERFACE_DESCRIPTOR_LENGTH,
			INTERFACE_DESCRIPTOR_TYPE,
			STREAMING_GENERAL,  // Audio class streaming interface
			LSB(audioControlSpec), MSB(audioControlSpec),
			LSB(totalStreamingIfaceLength), MSB(totalStreamingIfaceLength),


			// MIDI-in jack
			inputControlIfaceLength,
			INTERFACE_DESCRIPTOR_TYPE,
			CONTROL_INPUT_TERMINAL,
			0x01,  // Embedded (logical) jack type
			midiInJackId,
			0U,  // Jack string index: None


			// In endpoint descriptor
			streamingEndpointLength,
			ENDPOINT_DESCRIPTOR,
			PHY_TO_DESC(EPBULK_OUT),  // Endpoint address
			E_BULK | E_NO_SYNCHRONIZATION | E_DATA,  // Attributes
			LSB(MAX_PACKET_SIZE_EPBULK), MSB(MAX_PACKET_SIZE_EPBULK),
			0U,  // Polling interval in ms
			0x00,  // Refresh
			0x00,  // Sync endpoint number

			jackEndpointLength,
			ENDPOINT_DESCRIPTOR_TYPE,
			ENDPOINT_GENERAL,
			1U,  // Total embedded jacks
			midiInJackId  // Associated jack ID
		};
		return configurationDescriptor;
	}




	void UsbMidiDevice::start()
	{
		USBDevice::connect();
	}
}
