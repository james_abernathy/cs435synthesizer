/// @file
/// ::Project::Speakers class for periodically outputting buffered sound samples.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef PROJECT_SPEAKERS_HPP_
#define PROJECT_SPEAKERS_HPP_


#include "mbed/mbed.h"

#include "SoundBuffer.hpp"


namespace Project
{
	/// Periodically outputs buffered sound samples at a constant sampling rate.
	class Speakers
	{


		/// Period between PWM duty-cycle output updates in seconds.
	public:
		static constexpr float pwmUpdatePeriod_s_{SoundBuffer::samplePeriod_s_};

		/// Period between PWM cycles in seconds.
	public:
		static constexpr float pwmPeriod_s_{pwmUpdatePeriod_s_ * 1.0f};


		/// Frequency of PWM cycles in PWM pulses per second.
	public:
		static constexpr float pwmFrequency_{1.0f / pwmPeriod_s_};




		/// Pulse-width modulated output pin connected to the left speaker.
	private:
		PwmOut pwmLeft_;

		/// Pulse-width modulated output pin connected to the right speaker.
	private:
		PwmOut pwmRight_;

		/// Periodically calls #onPwmUpdate() once started with #start().
	private:
		Ticker pwmUpdateTicker_;


		/// Reference to the stereo sound sample buffer that this set of Speakers plays from.
	private:
		SoundBuffer &soundBuffer_;




		/// Initializes stereo speaker outputs to sample from @p soundBuffer at #pwmFrequency_ Hz.
		/// @param[in] pinLeft  mbed-defined pin constant to output the left channel
		///   of PWM output on.
		/// @param[in] pinRight  mbed-defined pin constant to output the right
		///   channel of PWM output on.
		/// @param[in] soundBuffer  Buffer of stereo sound samples to output from.
	public:
		Speakers(
			const PinName pinLeft,
			const PinName pinRight,
			SoundBuffer &soundBuffer);




		/// Updates the PWM analog outputs to the next buffered sample.
	private:
		void onPwmUpdate();




		/// Begins periodically writing new sound samples to these Speakers.
	public:
		void start();
	};
}


#endif  // !defined(PROJECT_SPEAKERS_HPP_)
