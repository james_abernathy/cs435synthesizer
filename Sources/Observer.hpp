/// @file
/// The ::Project::Observer class for receiving notifications from ::Project::Observable%s.
///
/// @copyright Copyright © 2014-2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#pragma once
#ifndef PROJECT_OBSERVER_HPP_
#define PROJECT_OBSERVER_HPP_


#include "Uncopyable.hpp"


/// A listener that receives notifications from ::Project::Observable%s.
/// @tparam TObservable  The child class of ::Project::Observable%s that should
///   be passed to this Observer by ::Project::Observable::notifyObservers().
template <typename TObservable>
class Observer :
	private Uncopyable
{


	/// Called by @p TObservable ::Project::Observable%s when broadcasting their observed event.
public:
	virtual void notify(
		const TObservable &observable) = 0;




public:
	virtual ~Observer()
	{
	}




	/// This protected constructor prevents directly instantiating an Observer base class.
protected:
	Observer()
	{
	}
};


#endif  // !defined(PROJECT_OBSERVER_HPP_)
