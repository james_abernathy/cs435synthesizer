/// @file
/// ::Project::Speakers class for periodically outputting buffered sound samples.
///
/// @copyright Copyright © 2018 James Abernathy.
///   This work is licensed under the Creative Commons Attribution 3.0 Unported License.
///   To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.


#include "Speakers.hpp"


namespace Project
{
	Speakers::Speakers(
		const PinName pinLeft,
		const PinName pinRight,
		SoundBuffer &soundBuffer
	) :
		pwmLeft_{pinLeft},
		pwmRight_{pinRight},
		pwmUpdateTicker_{},
		soundBuffer_{soundBuffer}
	{
		pwmLeft_.period(pwmPeriod_s_);
		pwmRight_.period(pwmPeriod_s_);
	}




	void Speakers::onPwmUpdate()
	{
		if (soundBuffer_.isEmpty()) {  // Awaiting newly written sound samples
			// THIS SHOULD NOT HAPPEN!
			// Writing samples too slowly like this will result in choppy sound.
			return;  // Skip updates until more samples are ready
		}

		float sampleLeft, sampleRight;
		soundBuffer_.readSample(sampleLeft, sampleRight);

		pwmLeft_.write(sampleLeft);
		pwmRight_.write(sampleRight);
	}




	void Speakers::start()
	{
		pwmUpdateTicker_.attach(
			callback(this, &Speakers::onPwmUpdate), pwmUpdatePeriod_s_);
	}
}
