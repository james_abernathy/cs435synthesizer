/* CS435 Project: MIDI-Controlled Synthesizer
** Program Written By: James Abernathy, Esra Bahceci, Angel Vazquez-Salazar
** File Written By: Angel Vazquez-Salazar
** Notes: This SampleBuffer file was intended to pre-define an array.
** Once this happens, we can reference the buffer in lieu of calculating
** the sine wave in real-time.
** We hope that this will speed up our program, and alleviate some of the stresses it currently has.
*/

#pragma once
#ifndef PROJECT_SAMPLEBUFFER_H
#define PROJECT_SAMPLEBUFFER_H

#include <cstdint>

#include "mbed/mbed.h"

namespace Project
{
	class SampleBuffer
	{
		protected:
			SampleBuffer(){}
		public:
			SampleBuffer(const float radians);
	}; // End of SampleBuffer class
} // End of Project class
#endif