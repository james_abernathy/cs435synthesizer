""" Displays and redirects MIDI input from the system to the Nucleo, even after
it restarts.

Requires the [`python-rtmidi` library](https://pypi.org/project/python-rtmidi/).

Installation: `pip install python-rtmidi` ([more info](
    https://spotlightkid.github.io/python-rtmidi/installation.html))
"""

__version__ = '1.0.0'

__author__ = 'James Abernathy'
__credits__ = []

__copyright__ = 'Copyright © 2018 James Abernathy, some rights reserved.'
__license__ = ('Creative Commons Attribution 3.0 Unported (CC BY 3.0)\n'
    + 'To view a copy of this license, visit <http://creativecommons.org/licenses/by/3.0/>.')




import rtmidi
import time




# Seconds between scans for input and output ports
MIDI_SCAN_PERIOD = 0.2




class _MidiInputHandler(object):
    """ Callback for an `rtmidi.MidiIn` instance, when a MIDI message is received.
    """

    def __init__(self, midi_out):
        """ Initializes a message handler for input from the MIDI-in port to be
        relayed on the `midi_out` (`rtmidi.MidiOut`) port.
        """
        self._midi_out = midi_out
        self._time_current = 0

    def __call__(self, event, callback_data=None):
        """ Outputs and redirects MIDI messages while the input and output
        channels are open.
        """
        midi_message, time_elapsed = event
        self._time_current += time_elapsed

        print('\t\t\t(@{:8.3f}) {!r}'.format(
            self._time_current, midi_message))

        # Forward message to output
        self._midi_out.send_message(midi_message)




class _MidiErrorHandler(object):
    """ Callback for an `rtmidi.MidiBase` instance, fired when an asynchronously
    generated error occurs on a child `rtmidi.MidiIn` or `rtmidi.MidiOut` instance.
    """

    def __init__(self, direction):
        self.direction = direction

    def __call__(self, error_type, error_message, callback_data=None):
        print('Error {!r} with {!s} port: {!s}'.format(
            error_type, self.direction.lower(), error_message))




def _print_port_options(direction, port_options):
    """ Prints a list `port_options` of currently available port names with the
    given `direction`.
    """

    direction = direction.lower()

    print()
    print('Current {!s} ports:'.format(
        direction))

    if not port_options:
        print('\tNone. Plug your {!s} device in and then re-run this script.'.format(
            direction))
        return

    for port_name in port_options:
        print('*\t{!r}'.format(
            port_name))




def _print_port_changes(direction, ports_old, ports_new):
    """ Prints the added and removed port names between `ports_old` and
    `ports_new` with the given `direction`.
    """

    direction = direction.capitalize()

    for port_name_disconnected in sorted(ports_old - ports_new):
        print('\t-[{:6s} {!r}]'.format(
            direction, port_name_disconnected))

    for port_name_connected in sorted(ports_new - ports_old):
        print('\t+[{:6s} {!r}]'.format(
            direction, port_name_connected))




def redirect_midi_forever(
    port_in_prefix=None,
    port_out_prefix=None
):
    """ Attempts to keep an input with `port_in_prefix` connected to an output
    named `port_out_prefix`, redirecting MIDI messages from one the former to the
    latter.
    """

    midi_in = rtmidi.MidiIn()
    midi_out = rtmidi.MidiOut()

    midi_in.set_error_callback(_MidiErrorHandler('input'))
    midi_out.set_error_callback(_MidiErrorHandler('output'))

    # Disable message filtering
    midi_in.ignore_types(
        sysex=False,
        timing=False,
        active_sense=False)

    if not port_in_prefix or not port_out_prefix:
        print('You must specify both --port_in_prefix="..." and --port_out_prefix="...".')
        _print_port_options('input', midi_in.get_ports())
        _print_port_options('output', midi_out.get_ports())
        return

    ports_in, ports_out = set(), set()  # Names of currently connected ports
    opened = False  # `True` when `midi_in` and `midi_out` are both opened
    port_in_opened, port_out_opened = None, None  # Names of matching ports

    # Monitor for input and output device connections and disconnections
    while True:
        ports_in_new = set(midi_in.get_ports())
        ports_out_new = set(midi_out.get_ports())

        # Print changes in connections
        _print_port_changes('input', ports_in, ports_in_new)
        _print_port_changes('output', ports_out, ports_out_new)

        ports_in, ports_out = ports_in_new, ports_out_new

        # Attach event listener when both the input and output are connected
        if opened:
            if (port_in_opened not in ports_in
                or port_out_opened not in ports_out
            ):  # Disconnected
                opened = False

                midi_in.close_port()
                midi_out.close_port()

                print('\t\t-[Redirection from {!r} to {!r}]'.format(
                    port_in_opened, port_out_opened))
                port_in_opened, port_out_opened = None, None

        else:  # Not opened
            port_in_match = next((port_name for port_name in sorted(ports_in)
                if port_name.startswith(port_in_prefix)), None)
            port_out_match = next((port_name for port_name in sorted(ports_out)
                if port_name.startswith(port_out_prefix)), None)

            if (port_in_match and port_out_match):  # Connected
                opened = True
                port_in_opened, port_out_opened = port_in_match, port_out_match

                midi_in.open_port(
                    midi_in.get_ports().index(port_in_opened),
                    name='NUCLEO Redirector In')
                midi_out.open_port(
                    midi_out.get_ports().index(port_out_opened),
                    name='NUCLEO Redirector Out')

                print('\t\t+[Redirection from {!r} to {!r}]'.format(
                    port_in_opened, port_out_opened))

                midi_in.set_callback(_MidiInputHandler(midi_out))

        time.sleep(MIDI_SCAN_PERIOD)




if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description='Redirects MIDI input from a source port to a destination.')
    parser.add_argument('--port_in_prefix',
        help='The name prefix of a MIDI device to read input from.')
    parser.add_argument('--port_out_prefix',
        help='The name prefix of a MIDI device to redirect input to.')

    redirect_midi_forever(**vars(parser.parse_args()))
