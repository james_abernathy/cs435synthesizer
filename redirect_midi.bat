@ECHO OFF
:: Runs redirect_midi.py to keep the NUCLEO connected to a MIDI keyboard.

python "redirect_midi.py" ^
	--port_in_prefix="Arturia MINILAB " ^
	--port_out_prefix="Synthesizer "

IF ERRORLEVEL 0 PAUSE
EXIT /B %ERRORLEVEL%
