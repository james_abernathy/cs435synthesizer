# CS 435 Embedded Systems Project

A MIDI synthesizer device to read serial MIDI commands and output stereo sound
using an STM32 microcontroller on [ST's NUCLEO-F401RE development board](
https://www.st.com/en/evaluation-tools/nucleo-f401re.html).




## Group Members

|  Name                        |  Bitbucket                                                                                                     |  Email                                                             |
|:-----------------------------|:--------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------|
|  Angel Ivan Vazquez-Salazar  |  [![Avatar](https://bitbucket.org/account/Angel-Vazquez/avatar/16)](https://bitbucket.org/Angel-Vazquez/)      |  [`vazqu053@cougars.csusm.edu`](email:vazqu053@cougars.csusm.edu)  |
|  Esra Bahceci                |  [![Avatar](https://bitbucket.org/account/esrabahceci/avatar/16)](https://bitbucket.org/esrabahceci/)          |  [`bahce001@cougars.csusm.edu`](email:bahce001@cougars.csusm.edu)  |
|  James Abernathy             |  [![Avatar](https://bitbucket.org/account/james_abernathy/avatar/32)](https://bitbucket.org/james_abernathy/)  |  [`abern002@cougars.csusm.edu`](email:abern002@cougars.csusm.edu)  |




## Firmware Components

*   **[`MidiIn`](https://bitbucket.org/james_abernathy/cs435project/src/master/Sources/MidiIn/)**:
    Feed it MIDI serial bytes and get callbacks when the MIDI state changes.

    *   [`State`](https://bitbucket.org/james_abernathy/cs435project/src/master/Sources/MidiIn/State.hpp):
        The top-level instance that interprets complete MIDI commands and
        remembers which notes should be turned on and how they should sound.
    *   [`MessageStream`](https://bitbucket.org/james_abernathy/cs435project/src/master/Sources/MidiIn/MessageStream.hpp):
        Feed it bytes from a live MIDI stream, and get notified when a whole
        MIDI message has arrived.


*   **Sound synthesis**: Fill buffers of sound samples based on what's in the
    `MidiIn::State` to be passed to analog output hardware.

    *   The analog outputs will be placed into a mode that samples from the
        buffer at a precise interval. Therefore, this component must be able
        to fill a buffer faster than it takes the digital-to-analog
        converter to empty one.
    *   Implement some of the following effects:
        *   Volume: A combination of channel volume, channel expression, and
            individual note pressure/aftertouch.
        *   Modulation: Oscillating the pitch up and 
        *   Pitch bend: Bending all notes on a channel higher or lower.
        *   Portamento: Swinging from note to note like a trombone.
        *   Envelope: Attack, Decay, Sustain, and Release (ADSR).
        *   Pan: Center, left, or right if we implement stereo analog output
            signals.


*   **MIDI input stream**: Receive MIDI stream bytes in some way.
    Three possibilities:

    1.  Receive the stream's raw bytes over USB-serial from the computer,
        which either forwards a keyboard's MIDI input or plays a MIDI file
        to the NUCLEO.
    2.  Implement a USB-MIDI interface on the STM32, which is functionally
        equivalent to USB-Serial but will appear and function as a MIDI
        instrument to the USB host computer (or keyboard).
    3.  Use actual MIDI connectors, and MIDI-to-USB dongles.


*   **Patch (instrument) sample library**: A library of soundwave samples, and
    methods to get a sample's value for a given time.

    *   Note: We are limited to 32KiB of ROM due to using the evaluation
        version of Keil µVision5. We will therefore have to use low sample
        rates, low sample resolution, or few instruments/patches.
